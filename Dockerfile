# Dockerfile
# https://semaphoreci.com/community/tutorials/dockerizing-a-python-django-web-application


# QUite hard to install on Alpine: lxml, numpy, and matplotlib. Hence, I Switched to ubuntu.
# FROM python:3.8-alpine
FROM python:3.8-slim


# install nginx
# RUN apt-get update && apt-get install nginx vim -y --no-install-recommends
# COPY nginx.default /etc/nginx/sites-available/default
# RUN ln -sf /dev/stdout /var/log/nginx/access.log \
#     && ln -sf /dev/stderr /var/log/nginx/error.log
# copy source and install dependencies
# COPY .pip_cache /opt/app/pip_cache/
#COPY . /opt/app/fabrikApi/
RUN mkdir -p /opt/app
RUN mkdir -p /opt/app/pip_cache
RUN mkdir -p /opt/app/fabrikApi
WORKDIR /opt/app
# COPY requirements.txt start-server-gunicorn.sh ./
COPY . .



# Install mysqlclient (must be compiled).
RUN apt-get update -qq \
    && apt-get install --no-install-recommends --yes \
        build-essential \
        default-libmysqlclient-dev \
        # Necessary for mysqlclient runtime. Do not remove.
        libmariadb3 \
    && rm -rf /var/lib/apt/lists/* \
    && python3 -m pip install --no-cache-dir mysqlclient \
    && apt-get autoremove --purge --yes \
        build-essential \
        default-libmysqlclient-dev

# Install packages that do not require compilation.
RUN python3 -m pip install --no-cache-dir \
      numpy scipy matplotlib lxml
      # pandas

# NUMPY:
# https://stackoverflow.com/questions/33421965/installing-numpy-on-docker-alpine

RUN pip install -r requirements.txt --cache-dir /opt/app/pip_cache; \
  rm -Rf /opt/app/pip_cache; \
  rm -Rf /opt/app/.pytest_cache; \
  rm -Rf /opt/app/.vscode; \
  rm -Rf /opt/app/.coveragerc; \
  chown -R www-data:www-data /opt/app

# add numpy
# RUN apk add --update --no-cache py3-numpy

# add scipy and matplotlib
# TODO: does not work here, right?
# TODO: change to debian-slim
# ENV PYTHONPATH=/usr/lib/python3.8/site-packages
# py3-scipy py3-numpy 

# add lxml (for posteditting of svg/xml)
# https://stackoverflow.com/questions/35931579/how-can-i-install-lxml-in-docker
# RUN apk add --no-cache --virtual .build-deps gcc libc-dev libxslt-dev && \
#     apk add --no-cache libxslt && \
#     pip3 install lxml>=3.5.0 && \
#     apk del .build-deps


# RUN apk add g++ jpeg-dev zlib-dev libjpeg make
# RUN pip3 install matplotlib
# TODO deinstall g++, again?

# RUN apk add g++ jpeg-dev zlib-dev libjpeg make
# RUN pip3 install matplotlib 
# matplotlib
# ENV PYTHONPATH=/usr/lib/python3.8/site-packages

# RUN apk update && apk add bash
RUN apt-get update
# start server
EXPOSE 8020
STOPSIGNAL SIGTERM
CMD ["/opt/app/start-server-gunicorn.sh"]