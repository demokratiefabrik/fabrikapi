drop procedure if exists procContentAggregates ;

create
    definer = `dbfabrikapi_cron`@`localhost`  procedure procContentAggregates() comment 'Calc Aggregates of content_progressions'
begin
    drop table if exists content_aggregates ;
    create table content_aggregates as
        SELECT
            content_id,
            COUNT(user_id) as progression_count,
            SUM(salience is not Null) as salience_count,
            AVG(salience)  as salience_avg,
            SUM(rating is not Null)  as rating_count,
            SUM(certainity is not Null)  as certainity_count,
            AVG(rating)  as rating_avg
        FROM
            content_progression
        WHERE (by_manager IS Null or by_manager = 0)
        GROUP BY content_id;
    UPDATE content
    INNER JOIN
        content_aggregates as agg ON content.id = agg.content_id
    SET
        content.agg_progression_count = agg.progression_count,
        content.agg_rating_avg = agg.rating_avg,
        content.agg_rating_count = agg.rating_count,
        content.agg_salience_avg = agg.salience_avg,
        content.agg_salience_count = agg.salience_count,
        content.agg_certainity_count = agg.certainity_count
    WHERE 1 = 1;

end;
