ALTER TABLE fabrikApi.content ADD agg_rating_avg FLOAT;
ALTER TABLE fabrikApi.content ADD agg_rating_count INT;
ALTER TABLE fabrikApi.stage_progression ADD date_last_day_session datetime;
ALTER TABLE fabrikApi.stage_progression ADD date_completed datetime;

ALTER TABLE fabrikApiCIR.content_peerreview ADD nof_criteria_accept4 INT;
ALTER TABLE fabrikApiCIR.content_peerreview ADD nof_criteria_accept5 INT;
ALTER TABLE fabrikApiCIR.content_peerreview ADD nof_criteria_accept6 INT;

ALTER TABLE fabrikApiCIR.content_peerreview_progression ADD criteria_accept4 INT;
ALTER TABLE fabrikApiCIR.content_peerreview_progression ADD criteria_accept5 INT;
ALTER TABLE fabrikApiCIR.content_peerreview_progression ADD criteria_accept6 INT;

content_peerreview_progression.criteria_accept4
content_peerreview_progression.criteria_accept4



alter table content
    modify rejected_peerreview_for_insert int null;
alter table content
    modify pending_peerreview_for_update int null;
alter table content
    modify pending_peerreview_for_insert int null;

alter table content_version
    modify completed_peerreview_for_insert int null;
alter table content_version
    modify rejected_peerreview_for_insert int null;
alter table content_version
    modify pending_peerreview_for_update int null;
alter table content_version
    modify pending_peerreview_for_insert int null;



alter table fabrikApiCIR_NEW.content
    modify pending_peerreview_for_insert int null;

alter table fabrikApiCIR_NEW.content
    modify rejected_peerreview_for_insert int null;

alter table fabrikApiCIR_NEW.content
    modify pending_peerreview_for_update int null;

alter table fabrikApiCIR_NEW.content
    modify completed_peerreview_for_insert int null;

alter table fabrikApiCIR_NEW.content
    drop constraint ck_content_pending_peerreview_for_insert;


UPDATE content SET pending_peerreview_for_insert = NULL WHERE pending_peerreview_for_insert = 0;
UPDATE content SET pending_peerreview_for_update = NULL WHERE pending_peerreview_for_update = 0;
UPDATE content SET rejected_peerreview_for_insert = NULL WHERE rejected_peerreview_for_insert = 0;
UPDATE content SET completed_peerreview_for_insert = NULL WHERE completed_peerreview_for_insert = 0;


UPDATE content
RIGHT JOIN content_peerreview ON content.id = content_peerreview.content_id
SET content.rejected_peerreview_for_insert = content_peerreview.id
WHERE rejected_peerreview_for_insert > 0 AND content_peerreview.id is not NULL   AND content_peerreview.operation = 'INSERT';


UPDATE content
RIGHT JOIN content_peerreview ON content.id = content_peerreview.content_id
SET content.completed_peerreview_for_insert = content_peerreview.id
WHERE completed_peerreview_for_insert > 0 AND content_peerreview.id is not NULL  AND content_peerreview.operation = 'INSERT';


UPDATE content
RIGHT JOIN content_peerreview ON content.id = content_peerreview.content_id
SET content.pending_peerreview_for_insert = content_peerreview.id
WHERE pending_peerreview_for_insert > 0 AND content_peerreview.id is not NULL   AND content_peerreview.operation = 'INSERT';


UPDATE content
RIGHT JOIN content_peerreview ON content.id = content_peerreview.content_id
SET content.pending_peerreview_for_update = content_peerreview.id
WHERE content.pending_peerreview_for_update > 0 AND content_peerreview.id is not NULL AND content_peerreview.operation = 'UPDATE' AND content_peerreview.approved = 0 AND content_peerreview.rejected = 0;
