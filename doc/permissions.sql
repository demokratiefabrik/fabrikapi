-- SHOW GRANTS FOR 'dbfabrikapi'@'10.0.1.0/255.0.0.0';
-- GRANT USAGE ON *.* TO `dbfabrikapi`@`10.0.1.0/255.0.0.0`;
GRANT SELECT, INSERT, UPDATE, DELETE ON `PROD_fabrikApi`.* TO `dbfabrikapi`@`172.21.0.0/255.255.0.0`;
GRANT SELECT, INSERT, UPDATE, DELETE ON `PROD_fabrikAuth`.* TO `dbfabrikauth`@`172.21.0.0/255.255.0.0`;
FLUSH PRIVILEGES ;