-- EXCEPT ASSEMBLY 2
mysqldump fabrikApiCIR > fabrikApiCIR2.sql


use PROD_fabrikApi;
truncate table log;
truncate table assembly_progression;
truncate table content_aggregates;
truncate table content_peerreview_progression;
truncate table content_version;
truncate table stage_progression;
truncate table user_aggregates;
truncate table notification;
truncate table user_aggregates_assembly_progression;
truncate table user_aggregates_content_response;
DELETE FROM   content_peerreview WHERE id > 0;
DELETE FROM stage WHERE stage.assembly_id <> 2;
DELETE FROM content_progression WHERE content_id > 0;
DELETE FROM content  WHERE content.contenttree_id IN (SELECT contenttree.id FROM contenttree WHERE assembly_id <> 2) AND id > 0;
DELETE FROM content WHERE content.type IN ('INVITED_COMMENT', 'COMMENT', 'FOLDER', 'UPDATEPROPOSAL', 'PRO', 'CONTRA');
DELETE FROM contenttree WHERE assembly_id <> 2;
DELETE FROM   user WHERE id > 1 and id not in (SELECT user_created_id FROM content);
DELETE FROM   assembly WHERE id <> 2;

-- Diable all accounts
-- diable all users (extempt moderators)
-- UPDATE PROD_fabrikAuth.auth_user SET is_active = 0 WHERE import_bulk = 'API_initiativeXY';
-- remove external_id
use PROD_fabrikAuth;
UPDATE auth_user SET is_active = 0, extra = NULL, email = '', username = CONCAT('DELETED_USER', id) WHERE id > 1064;
