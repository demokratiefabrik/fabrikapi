alter table content
    add subtree_content_id int null;
alter table content_version
    add subtree_content_id int null;


alter table assembly
    add custom_data json null;

alter table auth_user
    add custom_data json null;

alter table assembly_progression
    add custom_data json null;

alter table content
add constraint fk_condtent_subtree_content_id
    foreign key (subtree_content_id) references content (id)
        on delete cascade;



alter table user
    add content_certainity_count int default 0 null;

    alter table content_progression
    add certainity int null;
