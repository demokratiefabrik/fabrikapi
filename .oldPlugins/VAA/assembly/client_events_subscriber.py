
import random
from fabrikApi.util.contenttree_manager import ContentTreeManager
from fabrikApi.util.events import EventFrontendSubscriber, EventStageFocusContent
from pyramid.events import subscriber

from fabrikApi.views.lib.monitor_factories import factory_stage_tuple


@subscriber(EventFrontendSubscriber)
def client_event_subscriber(backend_event):
    """ Listen to events raised by frontend-client..."""

    request = backend_event.request
    response = backend_event.response
    event = backend_event.frontend_event

    if event.get('eventString') == "STAGE.RANDOM_SAMPLING_CONTENT":
        stagetuple = factory_stage_tuple(request, response, event)
        assert stagetuple, "stage tuples not yet transmitted"
        stage = stagetuple.get('stage')
        progression = stagetuple.get('progression')
        assert request.has_permission('observe', stage), "no oberve permission"

        assert stage.custom_data.get('RANDOM_FOCUS')
        excluded_from_random = stage.custom_data.get('EXCLUDE_FROM_RANDOM')
        contenttree_id = stage.contenttree_id

        treema = ContentTreeManager(request, contenttree=request.contenttree)
        topics = treema.load_topic_contents(
            contenttree_id, excluded_from_random)
        content = random.sample(topics, 1)[0]
        # Seek all topic contents...
        assert content
        progression.setFocusedContent(content)
        event['value'] = str(content.id)

        raiseEvent = EventStageFocusContent(
            request=request,
            stage=stage,
            response=response,
            progression=progression,
            content=content)
        request.registry.notify(raiseEvent)
