
from pyramid.events import subscriber
from fabrikApi.models.notification import initiate_notification
from fabrikApi.util.constants import NOTIFICATION_ACTION_PEERREVIEW_ASSIGNED
from fabrikApi.util.events import EventSessionFirstStageVisit, EventStageFocusContent
from fabrikApi.util.peerreview_manager import PeerreviewManager


@subscriber(EventStageFocusContent)
def event_raised_when_new_focused_content_is_received(event):

    # reorder of siblings if this is random-order content.
    assert event.request, "request is missing ---."
    assert event.response, "response object is missing"

    request = event.request

    if request.assembly.type_ != 'VAA':
        # IGNORE IF ITS NOT A CIR-ASSEMBLY
        return None

    # reorder of siblings if this is random-order content.
    assert event.request, "request is missing ---."
    assert event.stage, "stage is missing"
    assert event.content, "content is missing---."
    assert event.progression, "progression is missing---."

    # Update peerreview assignments....
    if not event.stage.custom_data or not event.stage.custom_data.get('RANDOM_FOCUS'):
        return None

    # continue only for delegates
    if not event.request.has_delegate_permission(event.request.assembly.identifier):
        return None

    # Assign Open Peerreviews to the user...
    peerreview_manager = PeerreviewManager(
        request=event.request,
        response=event.response,
        contenttree=event.stage.contenttree)
    nof_new_peerreviews = peerreview_manager.assign_open_peerreviews(
        event.stage, event.progression, event.content, prefer_current_stage=True)

    # add notification
    if nof_new_peerreviews > 0:
        notification = initiate_notification(
            request=event.request,
            action=NOTIFICATION_ACTION_PEERREVIEW_ASSIGNED,
            user_id=event.request.local_userid,
            assembly=event.request.assembly,
            stage=event.stage,
            contenttree_id=event.stage.contenttree_id,
            contenttree_identifier=event.content.contenttree_identifier,
            value=nof_new_peerreviews)

        # straight forward: return to user...
        if event.response:
            if not event.response.get('notifications'):
                event.response['notifications'] = {}
            event.response['notifications'][notification.id] = notification


@subscriber(EventSessionFirstStageVisit)
def notified_after_a_stage_has_been_entered_for_the_first_time(event):

    # reorder of siblings if this is random-order content.
    assert event.request, "request is missing ---."
    assert event.stage, "stage is missing"
    assert event.progression, "progression is missing---."

    request = event.request

    if request.assembly.type_ != 'VAA':
        # IGNORE IF ITS NOT A CIR-ASSEMBLY
        return None

    # patched required.
    event.stage.patch(event.request)

    # Reset focused content every day....
    event.progression.focused_content_id = None
