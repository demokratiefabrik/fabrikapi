#!/usr/bin/env python
"""
Assembly to compose a KI Modul.
"""


def patch(target):
    """ These methods/property are appended to DBAssembly with type CIR """

    target.MAX_DAILY_USER_COMMENTS = 10
    target.MAX_DAILY_USER_PROPOSALS = 3
    target.MAX_OVERALL_USER_PROPOSALS = 15
    # target.HIDE_MAIN_MENU_SECRETARIAT = True
    # target.HIDE_MAIN_MENU_PROFILE = True

    # When following overall threshold is reached, the daily limit is set to 1.
    target.TROTTLE_THRESHOLD_FOR_OVERALL_USER_PROPOSALS = 10

    if target.custom_data:
        if target.custom_data.get('PEERREVIEW_SELECTION_PROBABILITY'):
            target.PEERREVIEW_SELECTION_PROBABILITY = target.custom_data.get(
                'PEERREVIEW_SELECTION_PROBABILITY')
        if target.custom_data.get('PEERREVIEW_SELECTION_LIMITS_PER_DAY'):
            target.PEERREVIEW_SELECTION_LIMITS_PER_DAY = target.custom_data.get(
                'PEERREVIEW_SELECTION_LIMITS_PER_DAY')

    pass
