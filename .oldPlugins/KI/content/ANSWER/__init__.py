#!/usr/bin/env python
"""An Answer Item. Core Element Of PROs_AND_CONS (and others) modules. """
from pyramid.security import Deny

__all__ = ['patch', '__doc__']


def patch(target):
    """ These methods are appended to DBContent with type <modulename>"""

    # target.RATING_RANGE = range(-50, 51)  # Note: upper limit not included

    # PROPERTY OWNERSHIP
    target.COMMON_PROPERTY_CONTENT = False
    target.GIVEN_PROPERTY_CONTENT = False
    target.PRIVATE_PROPERTY_CONTENT = True

    def acl_extension_by_content_plugins(self, acl):
        """ Type-specific Permissions... (passed by Ref)"""
        acl.extend([
            (Deny, 'contributor@' +
             self.__assembly_identifier__, ['add']),
            (Deny, 'delegate@' +
             self.__assembly_identifier__, ['add'])
        ])

    target.acl_extension_by_content_plugins = acl_extension_by_content_plugins.__get__(
        target)
