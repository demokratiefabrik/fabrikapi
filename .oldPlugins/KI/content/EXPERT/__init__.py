#!/usr/bin/env python
"""
A EXPERT Statement Item. Core Element Of KI2022 modules..
"""

__all__ = ['patch', '__doc__']


def patch(target):
    """ These methods are appended to DBContent with type <modulename>"""

    # target.RATING_RANGE = range(-50, 51)  # Note: upper limit not included

    # PROPERTY OWNERSHIP
    target.COMMON_PROPERTY_CONTENT = False
    target.GIVEN_PROPERTY_CONTENT = True
    target.PRIVATE_PROPERTY_CONTENT = False

    pass
