#!/usr/bin/env python
"""
Compose a Textsheet structured by sections, subsections, paragraphs images, questions and answers.
"""

__all__ = ['patch', __doc__]


def patch(target):
    """ These methods are appended to DBStages with type TEXTSHEET"""

    target.DEFAULT_ICON = 'mdi-newspaper-variant-outline'

    target.SCHEDULE_ALERT_FREQUENCY_IN_DAYS = 99
    if target.custom_data:
        if target.custom_data.get('SCHEDULE_ALERT_FREQUENCY_IN_DAYS'):
            target.SCHEDULE_ALERT_FREQUENCY_IN_DAYS = target.custom_data.get(
                'SCHEDULE_ALERT_FREQUENCY_IN_DAYS')
    pass
