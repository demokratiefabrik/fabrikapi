#!/usr/bin/env python
"""
Compose a CIR_INFORMATION section.
"""

__all__ = ['patch', __doc__]


def patch(target):
    """ These methods are appended to DBStages with type INFORMATION"""

    target.DEFAULT_ICON = 'mdi-frequently-asked-questions'
    target.SCHEDULE_ALERT_FREQUENCY_IN_DAYS = 99

    if target.custom_data:
        if target.custom_data.get('SCHEDULE_ALERT_FREQUENCY_IN_DAYS'):
            target.SCHEDULE_ALERT_FREQUENCY_IN_DAYS = target.custom_data.get(
                'SCHEDULE_ALERT_FREQUENCY_IN_DAYS')

    pass
