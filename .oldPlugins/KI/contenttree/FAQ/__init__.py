#!/usr/bin/env python
"""
Compose a FAQ  structured by question, answer, comments.
"""

__all__ = ['patch', __doc__]


def patch(target):
    """ These methods are appended to DBContentTrees with type FAQ"""

    target.CONTENTTYPES = ['QUESTION', 'ANSWER', 'COMMENT', 'FOLDER']

    """ DEFINE Hierarchical Relations: What children types are allowed?  """
    target.ONTOLOGY = {
        None: ['QUESTION', ],
        'QUESTION': ['ANSWER', ],
        'ANSWER': ['COMMENT', 'FOLDER'],
        'COMMENT': ['COMMENT', 'FOLDER'],
    }

    # specify the content types that are priate property (can only be edited by owners)
    # given property (only managers can add and modify)
    # and common property (subject to peer review)
    target.PRIVATE_PROPERTY_CONTENT = ['COMMENT', 'ANSWER', 'QUESTION']
    target.GIVEN_PROPERTY_CONTENT = []

    # TODO: should appending question barrier be determined by stage position or by contenttree_id. If its contenttree_id  go via  contenttree.custom_data or so
    # if its stage position go via assembly.stages:
    # target.assembly.stages
    # allow_public_questions = True

    target.PLUGIN_CUSTOM_DATA['PUBLIC_QUESTIONS'] = True
    # if allow_public_questions:
    #     pass
