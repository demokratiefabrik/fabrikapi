#!/usr/bin/env python
"""
A Contenttree for running the KI Forum Module!
"""

__all__ = ['patch', __doc__]


def patch(target):
    """ These methods are appended to DBContentTrees with type KI_FORUM.
    # see __init__.py for detailed explanation.
    """

    target.CONTENTTYPES = ['SECTION', 'SUBSECTION', 'KI_CONTRA',
                           'KI_PRO', 'COMMENT', 'FOLDER']

    """ Define hierarchical Relations: What children types are allowed? Who are alloed to add, rate, modify,  them?  """
    target.ONTOLOGY = {
        None: ['SECTION', 'EXPERT'],
        'COMMENT': ['COMMENT', 'FOLDER'],
        'FOLDER': ['COMMENT', 'FOLDER'],

        # Textsheet
        'SECTION': ['SUBSECTION', ],
        'SUBSECTION': [],
        # 'PARAGRAPH': ['PARAGRAPH'],

        # KI ENTRIES
        'EXPERT': ['KI_PRO', 'KI_CONTRA'],
        'KI_PRO': ['COMMENT', 'FOLDER'],
        'KI_CONTRA': ['COMMENT', 'FOLDER']
    }
