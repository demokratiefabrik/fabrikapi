from fabrikApi.models.stage import DBStage
from fabrikApi.models import DBContent, DBContentTree, DBAssemblyProgression, get_assembly_by_identifier
from fabrikApi.models.user import DBUser

""" Quasi: Entry points to access API Objects """


class AssemblyFactory(object):

    def __init__(self, request):
        """ This method loads assembly into the request-object
        and checks object-speciic ACL (including object inheritance)
        """
        self.request = request

    def __getitem__(self, key):
        # TODO: test if diabled and stuff..

        self.request.assembly = get_assembly_by_identifier(self.request, key)
        self.request.assembly.setup_lineage(self.request)
        assert self.request.assembly, "invalid assembly specified."
        return (self.request.assembly)


class AssemblyFactoryPublic(object):

    def __init__(self, request):
        """ This method loads assembly into the request-object
        and checks object-speciic ACL (including object inheritance)
        """
        self.request = request

    def __getitem__(self, key):
        # TODO: only use id instead of key...
        self.request.assembly = get_assembly_by_identifier(self.request, key)
        self.request.assembly.setup_lineage(self.request)
        assert self.request.assembly, "invalid assembly specified."
        return self.request.assembly


class StageManagerFactory(object):

    def __init__(self, request):
        """ This method loads stage object into the request-object
        and checks object-specific ACLs (including object inheritance)
        """
        self.request = request

    def __getitem__(self, key):
        # TODO: only use id instead of key...
        stage = self.request.dbsession.query(DBStage).get(key)
        self.request.assembly = stage.assembly
        self.request.stage = stage
        # stage.patch()
        stage.setup_lineage(self.request)

        return self.request.stage


class ContentSubTreeManagerFactory(object):

    def __init__(self, request):
        """ This method loads contenttree object into the request-object
        and checks object-specific ACLs (including object inheritance)
        """
        self.request = request

    def __getitem__(self, key):

        # key == identifier: either a pure contenttree id (string)
        # or a composed string: contenttreeId-contentID, whereas contentid specifies the root of the subtree...
        subtree_content_id = None
        if '-' in key:
            contenttree_id, subtree_content_id = key.split('-')
        else:
            contenttree_id = key
        contenttree = self.request.dbsession.query(
            DBContentTree).get(int(contenttree_id))
        self.request.assembly = contenttree.assembly
        self.request.contenttree = contenttree
        contenttree.setup_lineage(self.request)

        if subtree_content_id:
            self.request.content = self.request.dbsession.query(
                DBContent).get(int(subtree_content_id))
            self.request.content.patch()
            return self.request.contenttree
        else:
            return self.request.contenttree


class ContentTreeManagerFactory(object):

    def __init__(self, request):
        """ This method loads contenttree object into the request-object
        and checks object-specific ACLs (including object inheritance)
        """
        self.request = request

    def __getitem__(self, key):
        contenttree = self.request.dbsession.query(DBContentTree).get(key)
        self.request.assembly = contenttree.assembly
        self.request.contenttree = contenttree
        # contenttree.patch()
        contenttree.setup_lineage(self.request)

        return self.request.contenttree


class ContentFactory(object):

    def __init__(self, request):
        """ This method loads content object into the request-object
        and checks object-specific ACLs (including object inheritance)
        """
        self.request = request

    def __getitem__(self, key):
        content = self.request.dbsession.query(DBContent).get(key)
        self.request.assembly = content.contenttree.assembly
        self.request.contenttree = content.contenttree
        self.request.content = content
        # content.patch()

        # TODO: contenttree must be patched...
        content.setup_lineage(self.request)
        return content


class AssemblyUserFactory(object):

    def __init__(self, request):
        """ This method loads user object into the request-object
        """
        self.request = request

    def __getitem__(self, key):

        assembly_user_id = self.request.matchdict.get('assembly_user_id')
        assembly_identifier = self.request.matchdict.get('assembly_identifier')

        self.request.assembly_user = self.request.dbsession.query(
            DBUser).get(assembly_user_id)
        self.request.assembly = get_assembly_by_identifier(
            self.request, assembly_identifier)

        assert self.request.assembly, 'INVALID ASSEMBLY'
        assert self.request.assembly_user, 'INVALID USER'

        # Is target user indeed part of this assembly?
        progression_query = self.request.dbsession.query(DBAssemblyProgression).filter(
            DBAssemblyProgression.assembly_id == self.request.assembly.id, DBAssemblyProgression.user_id == self.request.assembly_user.id)
        assert progression_query.count() == 1, 'target_user is not part of this assembly'
        self.request.assembly_progression = progression_query.one()

        self.request.assembly_user.setup_lineage(
            self.request, assembly_context=True)
        return self.request.assembly_user
