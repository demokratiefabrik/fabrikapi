""" Assemblies List View. """

import logging
from cornice.service import Service
from fabrikApi.models.assembly import DBAssembly
from fabrikApi.models.lib.mixins import arrow
from fabrikApi.models.log import DBLog

logger = logging.getLogger(__name__)


# SERVICES
assemblies = Service(cors_origins=('*',),
                     name='assemblies',
                     description='List Assemblies.',
                     path='/assemblies')


@assemblies.get(permission='public')
def get_assemblies(request):
    """Returns all assemblies which are either public or accessible by the current user.
    """

    # load all active assemblies
    # TODO: filter only active assemblies
    assemblies = request.dbsession.query(
        DBAssembly).filter(DBAssembly.disabled == 0).all()

    for assembly in assemblies:
        # assembly.patch()
        assembly.setup_lineage(request)

    # show only assemblies with at least view permission.
    assemblies = list(
        filter(lambda assembly: request.has_public_permission(assembly),
               assemblies)
    )

    assemblies = {v.identifier: v for v in assemblies}

    # log entry
    elog = DBLog(action='ANALYTICS')
    elog.value = "LOCALSTORAGE_INIT"
    elog.date_created = arrow.utcnow()
    request.dbsession.add(elog)

    return ({
        'assemblies': assemblies,
        'access_date': arrow.utcnow()
    })
