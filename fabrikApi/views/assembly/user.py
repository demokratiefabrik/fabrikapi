""" Assembly Read. """

import logging
import arrow
from get_docker_secret import get_docker_secret
from cornice.service import Service

from fabrikApi.util.events import EventViewAssemblyUser
from fabrikApi.views.lib.factories import AssemblyUserFactory

logger = logging.getLogger(__name__)

# SERVICES
service_assembly_user_id = Service(cors_origins=('*',),
                                   name='assembly_user', description='Read Assembly-User Profile.',
                                   path='/assembly/{assembly_identifier}/user/{assembly_user_id:\d+}/profile',
                                   traverse='/{assembly_user_id}',
                                   factory=AssemblyUserFactory)


@service_assembly_user_id.get(permission='public')
def get_profile(request):
    """Returns the public user-profile """

    assert request.assembly

    response = {'user': request.assembly_user,
                'configuration': {'t': int(get_docker_secret('fabrikapi_testing_phase', default=0)) == 1},
                'statistic': request.assembly_user.statistics,
                'access_date': arrow.utcnow(),
                'access_sub': request.authenticated_userid
                }

    if request.assembly.is_manager:
        response['locked'] = request.assembly_progression.locked

    raiseEvent = EventViewAssemblyUser(
        request=request,
        user=request.assembly_user,
        response=response)
    request.registry.notify(raiseEvent)

    return response
