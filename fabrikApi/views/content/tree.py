""" List all Content of a specific ContentTree """

import logging
from cornice.service import Service

from fabrikApi.util.contenttree_manager import ContentTreeManager
from fabrikApi.util.events import EventContentInteract
from fabrikApi.views.lib.factories import ContentSubTreeManagerFactory

logger = logging.getLogger(__name__)


service_content_tree = Service(
    cors_origins=('*',),
    name='content-subtree_route',
    description='List contenttree. (The whole tree or only a specific branch of the subtree...',
    path='/assembly/{assembly_identifier}/contenttree/{contenttree_identifier}/contenttree',
    traverse='/{contenttree_identifier}',
    factory=ContentSubTreeManagerFactory)


@service_content_tree.get(permission='public')
def tree_content(request):
    """Returns all content of a given contenttree."""
    assert request.contenttree.patched, "contenttree should be patched here..."
    # either public or registered users...
    assert request.local_userid or request.assembly.is_public

    # Load ContentTree Content
    subtree_content_id = request.content.id if request.content else None
    treema = ContentTreeManager(
        request, contenttree=request.contenttree, subtree_content_id=subtree_content_id)
    contenttree = treema.load_content()

    # Load available contenttree configurations
    configuration = request.contenttree.get_configuration(request)

    response = {
        'OK': True,
        'configuration': configuration,
        'contenttree': contenttree
    }

    # Notify Event
    if request.content:
        raiseEvent = EventContentInteract(
            request=request,
            response=response,
            content=request.content)
        request.registry.notify(raiseEvent)

    return (response)
