""" Content Views. CRUD & Ratings """
from pyramid.httpexceptions import HTTPBadRequest
from fabrikApi.models.lib.core import get_or_create_progression
from fabrikApi.models.contenttree.content import DBContentProgression
from fabrikApi.util.events import EventContentCreated, EventContentInteract, EventPeerReviewInitialized
import logging

from cornice.service import Service

from fabrikApi.models import DBContent
from fabrikApi.models.contenttree.content_peerreview import DBContentPeerReview, \
    initiate_peer_review
from fabrikApi.views.content.meta import content_schema, content_validator, \
    peerreview_content_insert_schema
from fabrikApi.views.lib.factories import ContentTreeManagerFactory
from fabrikApi.views.lib.helpers import remove_unvalidated_fields

logger = logging.getLogger(__name__)


service_content_create = Service(
    cors_origins=('*',),
    name='content_add',
    description='Create content.',
    path='/assembly/{assembly_identifier}/contenttree/{contenttree_id:\d+}/addcontent',
    traverse='/{contenttree_id}',
    factory=ContentTreeManagerFactory)


@service_content_create.post(
    schema=content_schema, validators=(content_validator,),
    permission='add', content_type="application/json")
def content_create(request):
    """Creates a new entry."""

    # SANITIZE JSON DATA
    jsoncontent = request.json_body['content']
    # remove unvalidated attributes from the content dict
    jsoncontent = remove_unvalidated_fields(jsoncontent, content_schema)

    # PREPARE DB OBJECT
    # Create a new content object (even if peer-review is still ongoing)
    newentry = DBContent(
        text=jsoncontent.get('text'),
        title=jsoncontent.get('title'),
        type_=jsoncontent.get('type'),
        contenttree_id=request.contenttree.id,
        user_id=request.local_userid,
        parent_id=jsoncontent.get('parent_id'))
    newentry.contenttree = request.contenttree
    # newentry.patch()
    newentry.setup_lineage(request)

    # EVERYTHING okay. Update DB
    request.dbsession.add(newentry)
    request.dbsession.flush()

    # update reviewer flag
    if request.assembly.is_manager:
        # No manager review necessary...
        newentry.reviewed = None
    else:
        newentry.reviewed = False

    # VALIDATE DATA:
    if newentry.parent_id:
        assert newentry.db_parent, "no parent could be loaded... (is it missing?)"
        newentry.subtree_content_id = newentry.db_parent.subtree_content_id
        newentry.db_parent.patch()
    else:
        # root elements
        if request.contenttree.IS_SUBTREE_CONTAINER:
            newentry.subtree_content_id = newentry.id

    assert newentry.validate_parent_and_type_data(), "content type is not valid..."
    assert newentry.path, "path is empty. "
    assert request.has_permission('add', newentry)

    # Create content progression
    progression = get_or_create_progression(
        request,
        DBContentProgression,
        user_id=request.local_userid,
        content_id=newentry.id)

    progression.set_read()

    response = {
        'OK': True,
        'content': {
            'path': newentry.path,
            'creator': request.current_user,
            'content': newentry,
            'progression': progression}
    }

    # Notify Event
    raiseEvent = EventContentCreated(
        request=request,
        content=newentry,
        response=response,
        progression=progression)
    request.registry.notify(raiseEvent)

    # # Notify Event
    raiseEvent = EventContentInteract(
        request=request,
        response=response,
        content=newentry)
    request.registry.notify(raiseEvent)

    return (response)


service_content_propose_insert = Service(
    cors_origins=('*',),
    name='content_propose_insert',
    description='Propose new content.',
    path='/assembly/{assembly_identifier}/contenttree/{contenttree_id:\d+}/propose',
    traverse='/{contenttree_id}',
    factory=ContentTreeManagerFactory)


@service_content_propose_insert.post(
    schema=peerreview_content_insert_schema,
    validators=(content_validator,),
    permission='propose_add',
    content_type="application/json")
def propse_content(request):
    """Propose a new entry."""

    # SANITIZE JSON DATA
    jsoncontent = request.json_body['content']
    jsoncontent = remove_unvalidated_fields(jsoncontent, content_schema)

    # PREPARE DB OBJECT
    # Create a new content object (even if peer-review is still ongoing)
    newentry = DBContent(
        # text=jsoncontent.get('text'),
        title=jsoncontent.get('title'),
        text=jsoncontent.get('text'),
        type_=jsoncontent.get('type'),
        contenttree_id=request.contenttree.id,
        user_id=request.local_userid,
        parent_id=jsoncontent['parent_id'])

    # try to add new content (as disabled)
    # establish a savepoint (to simulate modifications)
    request.dbsession.begin_nested()
    # VALIDATE DATA:
    request.dbsession.add(newentry)
    request.dbsession.flush()
    newentry.setup_lineage(request)
    val1 = newentry.validate_parent_and_type_data(), "content type is not valid..."
    val2 = newentry.path, "path is empty. "
    val3 = request.has_permission('propose_add', newentry)
    if not val3 or not val2 or not val1:
        request.dbsession.rollback()  # rolls back simulation
        raise HTTPBadRequest

    # PEER REVIEW HANDLER
    # Add PeerReview (Type: INSERT) Object
    # Content is marked as peerreviewd only after peerreview has been successfull)
    peerreview = initiate_peer_review(
        request,
        operation=DBContentPeerReview.INSERT,
        content=newentry,
        user_created_id=request.local_userid
        # Close peerreview flag after peer review has been passed.
    )

    newentry.pending_peerreview_for_insert = peerreview.id

    # Create content progression
    progression = get_or_create_progression(
        request,
        DBContentProgression,
        user_id=request.local_userid,
        content_id=newentry.id)

    response = {
        'OK': True,
        'content': {
            'path': newentry.path,
            'creator': request.current_user,
            'content': newentry,
            'progression': progression},
        'peerreview': {
            'creator': request.current_user,
            'content': newentry,
            'peerreview': peerreview}
    }

    # ADD COMMENT ENTRY: Entry to the contenttree to allow discussion...
    if jsoncontent.get('comment_title'):
        # and jsoncontent.get('comment_text')
        commentcontent = DBContent(
            title=jsoncontent.get('comment_title'),
            # text=jsoncontent.get('comment_text'),
            type_='COMMENT',
            contenttree_id=request.contenttree.id,
            user_id=request.local_userid,
            parent_id=newentry.id)
        request.dbsession.add(commentcontent)
        request.dbsession.flush()

        # Create content progression
        commentprogression = get_or_create_progression(
            request,
            DBContentProgression,
            user_id=request.local_userid,
            content_id=commentcontent.id)
        response.update({
            'comment': {
                'path': commentcontent.path,
                'creator': request.current_user,
                'content': commentcontent,
                'progression': commentprogression}
        })

    # Notify Event
    raiseEvent = EventPeerReviewInitialized(
        request=request,
        content=newentry,
        response=response,
        peerreview=peerreview
    )
    request.registry.notify(raiseEvent)

    # Notify Event
    raiseEvent = EventContentInteract(
        request=request,
        response=response,
        content=newentry)
    request.registry.notify(raiseEvent)

    return (response)
