""" EntryPoint for User Notifications  (List View). """

from get_docker_secret import get_docker_secret
from cornice.service import Service
from urllib.request import Request, urlopen
from pyramid.exceptions import HTTPNotFound

import logging
logger = logging.getLogger(__name__)


portainer = Service(
    cors_origins=('*',),
    name='portainer_hooks',
    description='Redirect Portainer Webhooks.',
    path='/{container}/webhooks/{nonce}')


@portainer.get()
@portainer.post()
def receive_deployment_notifications(request):
    """portainer Hooks """

    # container = request.matchdict.get('container')
    nonce = request.matchdict.get('nonce')

    if not nonce:
        raise HTTPNotFound()

    url_template = get_docker_secret(
        'gitlab_fabrikapi_deployment_notification_url')

    # logger.error('TEST NOTIFICAITON URL %s ' % url_template)
    if url_template:
        url = url_template.format(nonce=nonce)
        request = Request(url, method='POST')
        if not urlopen(request):
            raise HTTPNotFound()

    return ({'OK': True})
