""" List all PeerReviews within this assembly (also the rejected and archieved) """

import logging
from cornice.service import Service

from fabrikApi.util.peerreview_manager import PeerreviewManager
from fabrikApi.views.lib.factories import ContentTreeManagerFactory


logger = logging.getLogger(__name__)

service_peerreview_list = Service(
    cors_origins=('*',),
    name='peerreview_list',
    description='List Peerreview.',
    path='/assembly/{assembly_identifier}/contenttree/{contenttree_id:\d+}/peerreviews',
    traverse='/{contenttree_id}',
    factory=ContentTreeManagerFactory)


@service_peerreview_list.get(permission='public')
def peerreview_list(request):
    """Returns all content of a given contenttree."""

    assert request.local_userid or request.assembly.is_public

    assert request.contenttree.patched, "assembly should be patched here..."

    response = {
        'OK': True,
    }

    # Load Peerreviews
    peerreview_manager = PeerreviewManager(
        request=request,
        response=response,
        contenttree=request.contenttree)
    response['peerreviews'] = peerreview_manager.load_peerreviews()

    return (response)
