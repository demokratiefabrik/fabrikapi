""" EntryPoint for User Notifications  (List View). """

from sqlalchemy.sql.elements import and_
import logging
from cornice.service import Service
import jsonschema

from fabrikApi.models.notification import DBNotification


logger = logging.getLogger(__name__)


notifications = Service(
    cors_origins=('*',),
    name='notifications',
    description='Receive Notifcations for the user.',
    path='/notifications')


@notifications.post(permission='observe')
def receive_notifications(request):
    """USER MONITOR: CALL defs"""

    # SANITIZE JSON DATA
    schema = {
        "type": "object",
        "properties": {
            "update_date": {"type": "string"},
        },
        "required": []
    }

    # CONTINUEING ONLY WITH AUTHENTICATED USER
    assert request.local_userid, "invalid localuserid..."
    assert request.body, "invalid request body..."
    jsonschema.validate(request.json_body, schema)

    NOTIFICATION_LIMIT = request.registry.settings.get(
        'fabrikapi.notification.limit', 20)

    notifications = request.dbsession.query(DBNotification)\
        .filter(and_(
            DBNotification.user_id == request.local_userid))\
        .order_by(DBNotification.date_created.desc()).limit(NOTIFICATION_LIMIT).all()

    # # DEBUGGGGG
    # # TODO REMOVE
    # if len(notifications) > 0:
    #     notifications[0].call_action_when_initialized = True
    # print('-------------------')
    # print('------------------- REMOVE THAT')
    # print('-------------------')

    return ({'OK': True, 'notifications': notifications})
