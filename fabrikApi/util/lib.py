# -*- coding: utf-8 -*-
# Helper Functions

import logging
import arrow
import smtplib
from get_docker_secret import get_docker_secret

logger = logging.getLogger(__name__)


def convert_language_to_ISO639_1(code):
    """ Ensures that language code is in the format de-de (in particular important for frontend-processing)"""
    assert code

    splitter = code.lower()
    if splitter.find('_') > -1:
        splitter = code.split('_')
        splitter = '%s-%s' % (splitter[0], splitter[1])
    return splitter


def get_default_language_code(request):
    """ Get default language from the backend system: as specified in the ini files (default_locale_name)"""

    # get default language
    default_lang = 'de_CH'
    if request.registry.settings:
        default_lang = request.registry.settings.get(
            'default_locale_name', default_lang)

    return default_lang


def number_of_days_passed(datetime_):
    """ 
    returns the number of days passed, while considering a day to start and
    end at 03:00 UTC (in the morning)
    """
    daydiff = arrow.utcnow().shift(hours=-1).date() - \
        datetime_.shift(hours=-1).date()
    return (daydiff.days)


def date_with_midnightlag():
    """ 
    returns todays date, while considering a day to start and
    end at 03:00 UTC (in the morning)
    """
    # TODO: localtimezone adaption
    return arrow.utcnow().shift(hours=-3)


def email_notification(text, h1="Meldung aus der Demokratiefabrik", level="Error"):
    """ TODO: use pyramid mailer, ....once.... """

    EMAIL_HOST = get_docker_secret('email_host')
    HELPDESK_EMAIL = get_docker_secret(
        'email_helpdesk', 'dominik.wyss@ipw.unibe.ch')
    EMAIL_HOST_USER = get_docker_secret('email_from')
    EMAIL_PORT = get_docker_secret('email_port', 25)
    SERVER_DB_NAME = get_docker_secret('fabrikapi_db_host')

    if SERVER_DB_NAME == 'localhost':
        return None

    subject = "Demoratiefabrik-%s: %s" % (level, SERVER_DB_NAME)
    message = 'From: {}\nTo: {}\nSubject: {}\n\n{}\n\n{}'.format(
        EMAIL_HOST_USER, HELPDESK_EMAIL, subject, h1, text)

    with smtplib.SMTP(host=EMAIL_HOST, port=EMAIL_PORT) as smtpObj:
        # in case of further ascii errors: try message.encode('utf-8')
        # or message.as_string()
        smtpObj.sendmail(EMAIL_HOST_USER, HELPDESK_EMAIL,
                         message, mail_options=[], rcpt_options=[])
        # try: smtpObj.quit()

    # except smtplib.SMTPException :
    #     print(" Error : unable to send email .")
