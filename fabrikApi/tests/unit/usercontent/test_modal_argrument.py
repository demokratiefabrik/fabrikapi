from fabrikApi import models as models


def test_content_default():
    user = models.DBUser(phone="0796069985")
    content = models.DBContent(text="text", title="title", type="pro", user=user, parent_id=None,
                               parent_type=None, order=None, locked=False)
    assert content.title == 'title'
