""" List Profiles to Challenge. """

import logging
import arrow
from sqlalchemy import and_, func
from fabrikApi.plugins.CIR.constants import MONITOR_CONFRONT_WITH_STATEMENT
from cornice.service import Service
from fabrikApi.models.assembly import DBAssemblyProgression
from fabrikApi.models.contenttree.content import DBContent, DBContentProgression
from fabrikApi.models.log import DBLog
from fabrikApi.models.user import DBUser
from fabrikApi.views.lib.factories import StageManagerFactory

logger = logging.getLogger(__name__)

NUMBER_OF_SAMPLES = 3

# SERVICES
service_profiles_to_challenge = Service(
    cors_origins=('*',),
    name='profiles_to_challenge',
    description='Read List of random selected Assembly-User Profile to challenge.',
    path='/assembly/{assembly_identifier}/stage/{stage_id:\d+}/profilesToChallenge',
    traverse='/{stage_id}',
    factory=StageManagerFactory)


@service_profiles_to_challenge.get(permission='delegate')
def get_profile(request):
    """Returns a list of randomly selected user-profile to challenge """

    assert request.stage
    assert request.assembly

    # assert 'TOPIC_CONTENT_ID' in request.stage.custom_data
    statement_contenttree_id = request.assembly.custom_data['STATEMENT_CONTENTTREE_ID']
    assert statement_contenttree_id

    # NEW: HOW MANY STATEMENT IDS ARE ALREADY AVAILABLE....
    custom_data = request.dbsession.query(DBAssemblyProgression)\
        .filter(DBAssemblyProgression.assembly_id == request.assembly.id)\
        .all()
    data_with_statement_ids = list(
        filter(lambda s: s.custom_data and 'FOCUSED_CONTENT_ID' in s.custom_data and s.custom_data and s.custom_data['FOCUSED_CONTENT_ID'], custom_data))
    statement_content_ids = list(
        map(lambda s: s.custom_data['FOCUSED_CONTENT_ID'], data_with_statement_ids))

    # LOAD all personal cir statements...
    query_all = request.dbsession.query(DBContent, DBContentProgression, DBUser)\
        .join(DBUser, DBUser.id == DBContent.user_created_id)\
        .outerjoin(DBContentProgression, and_(
            DBContent.id == DBContentProgression.content_id,
            DBContentProgression.user_id == request.local_userid
        )
    )\
        .filter(
            DBContent.id.in_(statement_content_ids),
            DBUser.id != request.local_userid,
            DBContent.contenttree_id == statement_contenttree_id,
            DBContent.type_ == 'INVITED_COMMENT',
            DBContent.text != '',
            func.lower(DBContent.text) != 'test',
    )\
        .order_by(func.random())

    # 1) Load all unread contents (empty progression entry)
    nof = 3
    results = query_all.filter(
        DBContentProgression.id.is_(None)).limit(nof).all()
    # 2) add also the read contents if not enough unread are availbale
    if len(results) < 3:
        results += query_all.filter(
            DBContentProgression.id.isnot(None)).limit(nof-len(results)).all()

    # Compose Response object
    profiles = list(map(lambda result: {
        'statement_contenttree_identifier': result.DBContent.get_contenttree_identifier(),
        'user': result.DBUser
    }, results))

    # log all selected user profiles...
    for result in results:
        elog = DBLog(action='CONFRONT_WITH_STATEMENT')
        elog.assembly_id = request.assembly.id
        elog.content_id = result.DBContent.id
        elog.contenttree_id = statement_contenttree_id
        elog.action = MONITOR_CONFRONT_WITH_STATEMENT
        elog.user_id = request.local_userid
        elog.date_created = arrow.utcnow()
        elog.value = result.DBUser.id
        request.dbsession.add(elog)

    return profiles
