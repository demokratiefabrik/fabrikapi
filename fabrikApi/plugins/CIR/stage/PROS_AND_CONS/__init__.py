#!/usr/bin/env python
"""
Compose a PROS_AND_CONS section.
"""

__all__ = ['patch', __doc__]


def patch(target):
    """ These methods are appended to DBStages with type PROS_AND_CONS"""

    target.DEFAULT_ICON = 'mdi-frequently-asked-questions'

    pass
