# # import os
# from fabrikApi.models.contenttree.content import DBContent
# from fabrikApi.views.lib.helpers import remove_unvalidated_fields
# import openai
# from fabrikApi.views.lib.factories import ContentFactory, ContentTreeManagerFactory
# from cornice.service import Service
# from get_docker_secret import get_docker_secret
# from sqlalchemy_filters.filters import apply_filters

# # openai.api_key = "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"
# openai.api_key = get_docker_secret('fabrikapi_openai_secret_key')
# temperature = 0

# schema_argument_assessment = {
#     "type": "object",
#     "properties": {
#             "title": {"type": "string", "maxLength": 100},
#             "type": {"type": "string", "maxLength": 20},
#             "text": {"type": "string"}
#     },
#     "required": ["title", "text", "type"]
# }


# def clean_text(text: str):
#     return text.replace("\n", " ")


# service_argument_assessment = Service(
#     cors_origins=('*',),
#     name='openai_argument_assessment',
#     schema=schema_argument_assessment,
#     permission='contribute',
#     path='/assembly/{assembly_identifier}/contenttree/{contenttree_id:\d+}/argument_assessment',
#     traverse='/{contenttree_id}',
#     factory=ContentTreeManagerFactory)


# service_argument_assessment_edit = Service(
#     cors_origins=('*',),
#     name='openai_argument_assessment_edit',
#     schema=schema_argument_assessment,
#     permission='contribute',
#     path='/assembly/{assembly_identifier}/content/{content_id:\d+}/argument_assessment',
#     traverse='/{content_id}',
#     factory=ContentFactory)


# @service_argument_assessment.post(permission='public')
# @service_argument_assessment_edit.post(permission='public')
# def argument_assessment(request):
#     """
#     Return argument assessment
#     """
#     assert openai.api_key
#     jsoncontent = request.json_body['content']
#     topic = request.assembly.text_background
#     data = remove_unvalidated_fields(jsoncontent, schema_argument_assessment)
#     assert data['title'], "title must be specified."
#     assert data['text'], "title must be specified."
#     argument = f"""{data['title']}: {data['text']}"""

#     try:
#         response = {}
#         topicmatch = openai.Completion.create(
#             # model="text-curie-001",
#             model="text-davinci-003",

#             prompt=f"""
#             - Entscheide, ob ein Kommentar zur vorgegebenen Abstimmungsvorlage Bezug nimmt. Bitte antworte zuerst mit 'JA', 'NEIN', oder 'UNKLAR' und dann jeweils mit einer kurzen Erläuterung.

#             - Hintergrundinformation zur Abstimmungsvorlage: {clean_text(topic)}

#             - Kommentar: {clean_text(argument)}\n\n
#             """,
#             temperature=temperature,
#             max_tokens=200,
#             top_p=1.0,
#             frequency_penalty=0.0,
#             presence_penalty=0.0,
#             stop=["You:"]
#         )
#         response['topicmatch'] = topicmatch

#         sentiment = openai.Completion.create(
#             model="text-davinci-003",
#             # model="text-curie-001",

#             prompt=f"""
#             - Entscheide, ob ein Kommentar eher gegen oder für eine Abstimmungsvorlage spricht. Bitte antworte zuerst mit 'PRO', 'CONTRA', 'UNKLAR' und dann jeweils mit einer kurzen Erläuterung.

#             - Hintergrundinformation zur Abstimmungsvorlage: {clean_text(topic)}

#             - Argument: {clean_text(argument)}
#             """,
#             temperature=temperature,
#             max_tokens=200,
#             top_p=1.0,
#             frequency_penalty=0.0,
#             presence_penalty=0.0,
#             stop=["You:"]
#         )

#         response['sentiment'] = sentiment

#         # TODO: also for new content

#         parent_id = request.content.parent_id
#         contenttree_id = request.content.contenttree_id
#         filter_spec = [
#             {'model': 'DBContent', 'field': 'contenttree_id', 'op': '==',
#              'value': contenttree_id},
#             {'model': 'DBContent', 'field': 'id',
#              'op': '!=', 'value': request.content.id},
#             # {'model': 'DBContent', 'field': 'id',
#             #  'op': '<', 'value': request.content.id},
#             {'model': 'DBContent', 'field': 'type_',
#              'op': '==', 'value': request.content.type_},
#             {'model': 'DBContent', 'field': 'parent_id',
#              'op': '==', 'value': parent_id},
#             {'model': 'DBContent', 'field': 'deleted',
#              'op': '==', 'value': False},
#             {'model': 'DBContent', 'field': 'disabled',
#              'op': '==', 'value': False}
#         ]

#         # Load Contents: Which contents shall be loaded:
#         query = request.dbsession.query(DBContent)
#         query = apply_filters(query, filter_spec)
#         siblings_objects = query.all()
#         if len(siblings_objects) > 0:

#             siblings = "".join(
#                 list(map(
#                      lambda c: f"""
#                         Argument #{c.id}:  {clean_text(c.title)}: {clean_text(c.text)}

#                         """,
#                      siblings_objects
#                      )))
#             siblings_ids = ", ".join(
#                 list(map(lambda c: f"#{c.id}", siblings_objects))
#             )

#             # Abstimmungsvorlage: «{topic}»\n\n
#             prompt = f"""
#                 Es geht um folgende Abstimmungsvorlage: {clean_text(topic)}

#                 Zu dieser Vorlage habe ich schon einige Argumente zusammengetragen:

#                 {siblings}

#                 Nun soll noch ein neues Argument hinzukommen:

#                 Argument #000000: {clean_text(argument)}

#                 Entscheide, ob der vom neuen Argument (#000000) aufgegriffene Gedanke bereits in anderen Argumenten {siblings_ids} vorkommt. Bitte antworte zuserst mit 'JA', 'NEIN', oder 'UNKLAR' sowei einer Erläuterung deiner Entscheidung. Bei 'JA', nenne mir auch die entsprechende Nummer des redundanten Argumentes.

#                 """

#             redundancy = openai.Completion.create(
#                 model="text-davinci-003",
#                 # model="text-curie-001",

#                 prompt=prompt,
#                 temperature=temperature,
#                 max_tokens=200,
#                 top_p=1.0,
#                 frequency_penalty=0.0,
#                 presence_penalty=0.0,
#                 stop=["You:"]
#             )

#             # print(prompt)
#             response['redundancy'] = redundancy

#         if request.content:
#             assert request.content.text
#             prompt = f"""
#                     - Hintergrundinformation zur Abstimmungsvorlage: {clean_text(topic)}

#                     - Entscheide, ob zwei Kommentare ungefähr den selben Gedanken zu einer Abstimmungsvorlage beschreiben. Bitte antworte mit 'JA', 'NEIN', oder 'UNKLAR' sowie  einer kurzen Erläuterung.

#                     - 1. Kommentar: {clean_text(argument)}

#                     - 2. Kommentar: {clean_text(request.content.text)}
#                     """
#             similarity = openai.Completion.create(
#                 model="text-davinci-003",
#                 # model="text-curie-001",

#                 prompt=prompt,
#                 temperature=temperature,
#                 max_tokens=200,
#                 top_p=1.0,
#                 frequency_penalty=0.0,
#                 presence_penalty=0.0,
#                 stop=["You:"]
#             )

#         # print(prompt)
#         response['similarity'] = similarity
#     except openai.error.RateLimitError:
#         pass
#     except openai.error.ServiceUnavailableError:
#         pass

#     return response
