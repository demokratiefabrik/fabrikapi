#!/usr/bin/env python
"""
User-generated and rather unspecified content (private property).
"""


__all__ = ['patch', '__doc__']


def patch(target):
    """ These methods are appended to DBContent with type <modulename>"""

    # does the content belongs to the collective (and not to the creater?)
    # it comes in fixed order: column: <order_position>
    target.is_in_random_order = True

    # PROPERTY OWNERSHIP
    target.COMMON_PROPERTY_CONTENT = False
    target.GIVEN_PROPERTY_CONTENT = False
    target.PRIVATE_PROPERTY_CONTENT = True

    # target.DEFAULT_CONTENT_TYPE = 'COMMENT'

    pass
