#!/usr/bin/env python
"""An Question Item. Core Element Of PROs_AND_CONS (and others) modules. """
from pyramid.security import Deny

__all__ = ['patch', '__doc__']


def patch(target):
    """ These methods are appended to DBContent with type <modulename>"""

    # target.RATING_RANGE = range(-50, 51)  # Note: upper limit not included

    # PROPERTY OWNERSHIP
    target.COMMON_PROPERTY_CONTENT = False
    target.GIVEN_PROPERTY_CONTENT = False
    target.PRIVATE_PROPERTY_CONTENT = True

    def acl_extension_by_content_plugins(self, acl):
        """ Type-specific Permissions... (passed by Ref) """
        # assert 'PUBLIC_QUESTIONS' in self.contenttree.PLUGIN_CUSTOM_DATA
        if 'PUBLIC_QUESTIONS' in self.contenttree.PLUGIN_CUSTOM_DATA.keys():
            allow_public_questions = self.contenttree.PLUGIN_CUSTOM_DATA['PUBLIC_QUESTIONS']
            if not allow_public_questions:
                acl.extend([
                    (Deny, 'delegate@' +
                     self.__assembly_identifier__, ['add'])
                ])

        # Experts should not be able to add questions...
        acl.extend([
            (Deny, 'contributor@' +
             self.__assembly_identifier__, ['add']),
            (Deny, 'expert@' +
                self.__assembly_identifier__, ['add']),
        ])

    target.acl_extension_by_content_plugins = acl_extension_by_content_plugins.__get__(
        target)
