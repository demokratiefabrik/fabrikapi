#!/usr/bin/env python
"""
Subtree: This is the root object of a next subtree. This content is normally loaded only with the subtree.
But not loaded with the main tree.
"""

__all__ = ['patch', '__doc__']


def patch(target):
    """ These methods are appended to DBContent with type <modulename>"""

    # it comes in fixed order: column: <order_position>
    target.is_in_random_order = True

    # PROPERTY OWNERSHIP
    target.COMMON_PROPERTY_CONTENT = False
    target.GIVEN_PROPERTY_CONTENT = True
    target.PRIVATE_PROPERTY_CONTENT = False

    pass
