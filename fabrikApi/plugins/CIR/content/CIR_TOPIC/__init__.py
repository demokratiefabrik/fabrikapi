#!/usr/bin/env python
"""
A CIR-TOPIC Item. Core Element Of CIR-Voice modules. The topic/vote-issue everything is about.
"""

__all__ = ['patch', '__doc__']


def patch(target):
    """ These methods are appended to DBContent with type <modulename>"""

    # target.RATING_RANGE = range(-50, 51)  # Note: upper limit not included

    # PROPERTY OWNERSHIP
    target.COMMON_PROPERTY_CONTENT = False
    target.GIVEN_PROPERTY_CONTENT = True
    target.PRIVATE_PROPERTY_CONTENT = False

    pass
