#!/usr/bin/env python
"""A CONTRA Item. Core Element Of PROs_AND_CONS modules. """

__all__ = ['patch', '__doc__']


def patch(target):
    """ These methods are appended to DBContent with type <modulename>"""

    # target.RATING_RANGE = range(-50, 51)  # Note: upper limit not included

    # PROPERTY OWNERSHIP
    target.COMMON_PROPERTY_CONTENT = True
    target.GIVEN_PROPERTY_CONTENT = False
    target.PRIVATE_PROPERTY_CONTENT = False

    target.CHANGE_ORDER_DAILY = True

    pass
