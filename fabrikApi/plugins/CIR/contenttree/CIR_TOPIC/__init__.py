#!/usr/bin/env python
"""
A Contenttree for running the voice Module: Container for the TOpic Content!
"""

__all__ = ['patch', __doc__]


def patch(target):
    """ These methods are appended to DBContentTrees with type CIR_TOPIC.
    # see __init__.py for detailed explanation.
    """

    target.CONTENTTYPES = ['CIR_TOPIC', 'COMMENT', ]

    """ Define hierarchical Relations: What children types are allowed? Who are alloed to add, rate, modify,  them?  """
    target.ONTOLOGY = {
        None: ['CIR_TOPIC'],
        'CIR_TOPIC': ['COMMENT', 'INVITED_COMMENT'],
        'COMMENT': ['COMMENT', ],
        'INVITED_COMMENT': ['COMMENT', ],
    }
