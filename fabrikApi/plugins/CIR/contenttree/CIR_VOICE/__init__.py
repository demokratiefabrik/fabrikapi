#!/usr/bin/env python
"""
A Contenttree for running the voice Module: Everybody can speak up!
"""

__all__ = ['patch', __doc__]


def patch(target):
    """ These methods are appended to DBContentTrees with type CIR_VOICE.
    # see __init__.py for detailed explanation.
    """

    target.CONTENTTYPES = ['COMMENT', 'CIR_TOPIC', 'INVITED_COMMENT', 'FOLDER']
    target.IS_SUBTREE_CONTAINER = True

    """ Define hierarchical Relations: What children types are allowed? Who are alloed to add, rate, modify,  them?  """
    target.ONTOLOGY = {
        None: ['INVITED_COMMENT'],
        'COMMENT': ['COMMENT', 'FOLDER'],
        'FOLDER': ['COMMENT', 'FOLDER'],
        'INVITED_COMMENT': ['COMMENT', 'FOLDER'],
    }
