

from fabrikApi.models.assembly import get_assembly_progression_of_current_user
from fabrikApi.models.contenttree.content import DBContent
from fabrikApi.plugins.CIR.assembly.lib import get_personal_statement
from fabrikApi.util.events import EventFrontendSubscriber, EventAssemblyFocusContent
from pyramid.events import subscriber

# from fabrikApi.models.lib.core import get_or_create_progression
# from fabrikApi.models.stage import DBStageProgression
# from .lib import get_personal_statement


@subscriber(EventFrontendSubscriber)
def client_event_subscriber(backend_event):
    """ Listen to events raised by frontend-client..."""

    request = backend_event.request
    response = backend_event.response
    event = backend_event.frontend_event

    # Create / or assigns the personal statement to the currently focues content in the stage progression...
    if event.get('eventString') == "ASSEMBLY.AUTO_CREATE_FOCUSED_CONTENT":
        assert request.has_permission(
            'delegate', request.assembly), "no delegate permission"

        newsubtree = get_personal_statement(
            request, request.local_userid)
        if newsubtree:
            return True

        # CREATE INVITED_COMMENT
        statement_contenttree_id = request.assembly.custom_data['STATEMENT_CONTENTTREE_ID']
        assert statement_contenttree_id, 'Assembly not properly configured: STATEMENT_CONTENTTREE_ID is missing.'
        # Create blank Subtree content
        newsubtree = DBContent(
            type_='INVITED_COMMENT',
            contenttree_id=statement_contenttree_id,
            user_id=request.local_userid)
        request.dbsession.add(newsubtree)
        request.dbsession.flush()
        assert newsubtree.id
        newsubtree.subtree_content_id = newsubtree.id

        # save value in assembly progression
        progression = get_assembly_progression_of_current_user(request)
        progression.set_focused_content(newsubtree)

        # Notify everybody interested
        # Create stage progression
        assert progression
        event['value'] = str(newsubtree.id)
        raiseEvent = EventAssemblyFocusContent(
            request=request,
            # stage=progression.stage,
            response=response,
            progression=progression,
            content=newsubtree)
        request.registry.notify(raiseEvent)
