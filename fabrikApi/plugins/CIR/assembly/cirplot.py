""" CIRPLOT Plotter. """

import logging
import random
from scipy import stats
import numpy as np
from fabrikApi.models.contenttree.content import DBContentProgression
import lxml.etree as ET
from lxml.html.clean import Cleaner
from fabrikApi.models.user import DBUser

cirplot_seed = 984

# HOW TO CLEAN SVG AT THE END... (to prevent CSP errors)
cleaner = Cleaner()
cleaner.style = True
cleaner.inline_style = True
cleaner.javascript = False
cleaner.scripts = False
cleaner.links = False
cleaner.meta = False
cleaner.page_structure = False
cleaner.processing_instructions = False
cleaner.forms = False
cleaner.remove_unknown_tags = False
cleaner.safe_attrs_only = False

logger = logging.getLogger(__name__)


# Get pure random data
def get_random_data(request):
    distribution = request.GET.get('distribution', 'uniform')
    number = int(request.GET.get('maxNumber', 300))
    # maxNumber = int(request.GET.get('maxNumber', 300))
    # number = number / maxNumber
    # number = int(round(number / maxNumber, 0))

    def uniform(x):
        return x*100

    def skewedSlight(x):
        return x**(1.15)*100

    def skewedNormal(x):
        return x**(1.35)*100

    def polarized(x, exp):
        side = int(round(x*1000)) % 2
        x = x**exp*100
        if side == 1:
            return 100-x
        return x

    def polarizedSlight(x):
        return polarized(x, 1.7)

    def polarizedStrong(x):
        return polarized(x, 1)

    rvars = np.random.rand(number)

    # rvars = random.sample(range(0, 100), number)

    if distribution == 'skewed-slight':
        transform = skewedSlight
    elif distribution == 'skewed-normal':
        transform = skewedNormal

    # scipy: not installed (TODO)
    elif distribution == 'skewed-strong':
        transform = uniform
        rvars = stats.beta.rvs(5, 1, loc=0, scale=1, size=number)
    elif distribution == 'polarized-strong':
        transform = polarizedStrong
        rvars = stats.beta.rvs(8, 0.5, loc=0, scale=1, size=number)
    elif distribution == 'polarized-slight':
        transform = polarizedSlight
    else:
        transform = uniform

    data = []
    for i in rvars:
        pos = transform(i)
        data.append(
            {'id': i, 'pos': pos, 'label': "User (%s)" % round(pos, 2)}
        )

    return (None, data)


def loadAssemblyUsers(request, content_id,
                      exclude_missing_ratings=True,
                      exclude_missing_statements=False,
                      maxNumber=None):

    # Query
    query = request.dbsession.query(DBContentProgression, DBUser)\
        .join(DBUser, DBUser.id == DBContentProgression.user_id)\
        .filter(DBContentProgression.content_id == content_id)\
        .filter(DBContentProgression.user_id != request.local_userid)

    if exclude_missing_ratings:
        query = query.filter(DBContentProgression.rating.isnot(None))

    # Random Ordering (for filtering top maxNumber)
    random.seed(request.get_user_specific_random_seed + cirplot_seed)
    randomNumber = random.randint(1, 10)
    query = query.order_by((DBUser.id % randomNumber).desc())
    results = query.all()

    totalUserCount = len(results)
    if maxNumber:
        results = results[:maxNumber]

    # add current user
    ownEntry = request.dbsession.query(DBUser, DBContentProgression)\
        .join(DBUser, DBUser.id == DBContentProgression.user_id)\
        .filter(
            DBContentProgression.user_id == request.local_userid,
            DBContentProgression.content_id == content_id).first()
    if ownEntry:
        totalUserCount += 1
        results.append(ownEntry)

    # again: order by date of participation...
    results.sort(key=lambda x: x.DBUser.id)

    return (totalUserCount, results)


def get_user_data(request, currentUserID):

    # TODO: only temp...
    # distribution = request.GET.get('distribution', 'uniform')
    content_id = request.assembly.custom_data.get(
        'TOPIC_CONTENT_ID', None)
    assert content_id
    maxNumber = int(request.GET.get('maxNumber', 300))

    totalUserCount, results = loadAssemblyUsers(
        request, content_id, maxNumber=maxNumber)

    data = list(map(lambda x: {'id': x.DBUser.id, 'pos': x.DBContentProgression.rating,
                'label': x.DBUser.username}, results))

    # DEMO IMPORT
    # importData(request)

    # CUSTOM DATA: custom_data.get('FULLNAME')
    return totalUserCount, data


def xmlPostModifications(xml, users, currentUserID):
    """ XML POST-MODIFICATIONS
    Matplotlib ist not very good, in formatting SVGs
    """

    # following nodes are renderes as last (for on-top/vertical ordering.)
    # selectedNodeEls = []

    # set 100% size
    highlightedNodes = []
    root = ET.fromstring(xml)  # Convert to XML Element Templat
    root.attrib['width'] = '100%'
    root.attrib.pop('height')

    # Add interactivity to dot-nodes
    # TODO: do more efficient xpath...
    scatgrid = root.find('.//*[@id="scatgrid"]')
    assert scatgrid is not None
    nodes = scatgrid.findall('.//*[@clip-path]/*')
    assert len(nodes) == len(
        users), 'not all dots could be plotted. Did you remove all missings before?'
    for i in range(len(nodes)):
        user = users[i]
        node = nodes[i]
        # TODO: assert that order is correct....!!!!
        node.attrib['data-id'] = "%s" % user['id']  # Temporary
        node.attrib['data-label'] = "%s" % user['label']  # Temporary
        node.attrib['data-pos'] = "%s" % user['pos']  # Temporary
        if user['id'] == currentUserID:
            node.attrib['class'] = "current"  # Mark current user
            highlightedNodes.append(node)

        # extract fill color in dedicated attribute
        if node.attrib.get('style'):
            attrs = {pair.split(":")[0]: pair.split(
                ":")[1] for pair in node.attrib.get('style').split(";")}
            if attrs.get('fill'):
                node.attrib['data-fill'] = "%s" % attrs.get('fill').strip()

        # # z-index hack: bring selected Nodes to the front
        for sel in highlightedNodes:
            g = sel.getparent()
            scatgrid.append(g)

    content = ET.tostring(root,  xml_declaration=True, encoding="UTF-8")
    # CSP save (removes all styles and css's)
    content = cleaner.clean_html(content)

    return content
