from fabrikApi.models.assembly import get_assembly_progression_of_current_user
from fabrikApi.models.contenttree.content import DBContent, DBContentProgression
import logging

logger = logging.getLogger(__name__)


def get_personal_statement(request, user_id):

    statement_contenttree_id = request.assembly.custom_data.get(
        "STATEMENT_CONTENTTREE_ID")
    assembly_progression = get_assembly_progression_of_current_user(
        request, auto_create=False, events=False, custom_user_id=user_id)
    if not assembly_progression.custom_data:
        return None
    statement_content_id = assembly_progression.custom_data.get(
        "FOCUSED_CONTENT_ID") if assembly_progression.custom_data else None
    if not statement_content_id:
        return None

    content = request.dbsession.query(DBContent).get(statement_content_id)
    assert content.contenttree_id == statement_contenttree_id
    assert content.user_created_id == user_id
    assert content.type_ == 'INVITED_COMMENT'
    assert content.parent_id is None
    return content


def is_personal_statement(request, statement_contenttree_id, content):
    """ is the content the current users' personal statement?"""

    assembly_progression = get_assembly_progression_of_current_user(
        request, auto_create=False, events=False, custom_user_id=request.local_userid)
    statement_content_id = assembly_progression.custom_data.get(
        "FOCUSED_CONTENT_ID") if assembly_progression.custom_data else None
    return content \
        and content.contenttree_id == statement_contenttree_id \
        and not content.parent_id \
        and statement_content_id \
        and statement_content_id == content.id \
        and request.local_userid == content.user_created_id


def get_personal_rating(request, user_id):
    """
    Return the topic-rating (public statement) of the given user...
    returns None if no statement is available...
    """

    custom_data = request.assembly.custom_data
    topic_content_id = custom_data['TOPIC_CONTENT_ID']
    assert topic_content_id, 'Assembly not properly configured: TOPIC_CONTENT_ID is missing.'
    topic = request.dbsession.query(DBContentProgression, DBContent).join(DBContent).filter(
        DBContentProgression.user_id == user_id, DBContentProgression.content_id == topic_content_id).first()
    if topic and topic.DBContentProgression:
        return topic.DBContentProgression.rating
    return None


def compose_public_cir_profile(
        request, user_id, response,
        add_main_topic_entry=True,
        add_best_rated_argument_ids=True,
        add_public_cir_profile_statement=True):

    # Add Main TopicContent Entry!
    if add_main_topic_entry and user_id != request.local_userid:
        # Get progression entries for other users.
        # (No need to get the progression entry of the current user)
        custom_data = request.assembly.custom_data
        topic_content_id = custom_data['TOPIC_CONTENT_ID']
        assert topic_content_id, 'Assembly not properly configured: TOPIC_CONTENT_ID is missing.'
        topic = request.dbsession.query(DBContentProgression, DBContent).join(DBContent).filter(
            DBContentProgression.user_id == user_id, DBContentProgression.content_id == topic_content_id).first()
        if topic:
            topic.DBContent.patch()
            topic.DBContent.contenttree.patch()
            response.update({
                'topic_tuple': {
                    'content': topic.DBContent,
                    'progression': {
                        'salience': topic.DBContentProgression.salience if topic.DBContentProgression else None,
                        'certainity': topic.DBContentProgression.certainity if topic.DBContentProgression else None,
                        'rating': topic.DBContentProgression.rating if topic.DBContentProgression else None
                    }}})

        else:
            response.update({'topic_tuple': None})
            logger.debug('NO TOPIC-DBContentProgression found for this user')

    # Add id of personal statement content
    if add_public_cir_profile_statement:
        custom_data = request.assembly.custom_data
        statement_contenttree_id = custom_data['STATEMENT_CONTENTTREE_ID']
        assert statement_contenttree_id, 'Assembly not properly configured: STATEMENT_CONTENTTREE_ID is missing.'
        statement = get_personal_statement(request, user_id)
        if statement:
            response.update({
                'statement_contenttree_identifier': "%s-%s" % (statement_contenttree_id, statement.id),
                'statement_content_id': statement.id,
                'statement_contenttree_id': statement_contenttree_id
            })

    # Add Main TopicContent Entry!
    if add_best_rated_argument_ids:
        # Get progression entries for other users.
        # (No need to get the progression entry of the current user)
        custom_data = request.assembly.custom_data
        contenttree_id = custom_data['PROS_AND_CONS_CONTENTTREE_ID']
        if not contenttree_id:
            raise Exception(
                'Misconfiguration: PROS_AND_CONS_CONTENTTREE_ID is missing in assembly configuration.')

        arguments = request.dbsession.query(DBContent, DBContentProgression).join(DBContentProgression)\
            .filter(
                DBContent.contenttree_id == contenttree_id,
                DBContent.parent_id.is_(None),
                DBContentProgression.rating.isnot(None),
                DBContentProgression.user_id == user_id
        )\
            .order_by(DBContentProgression.rating.desc())\
            .all()

        # Patch data...
        if len(arguments) > 0:
            arguments[0].DBContent.contenttree.patch()

        argument_tuples = []
        for tuple in arguments:
            tuple.DBContent.patch()
            # filtered[tuple.DBContent.type_].append(tuple.DBContentProgression)
            argument_tuples.append({
                'content': tuple.DBContent,
                'progression': {'rating': tuple.DBContentProgression.rating}
            })

        response.update({
            'arguments': argument_tuples
            # 'argumentarium': filtered['PRO'][0:3] + filtered['CONTRA'][0:3]
        })
