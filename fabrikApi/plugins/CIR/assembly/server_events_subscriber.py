
import arrow
from pyramid.events import subscriber

from fabrikApi.models.assembly import get_assembly_progression_of_current_user
from fabrikApi.models.contenttree.content import DBContent
from fabrikApi.models.notification import initiate_notification
from fabrikApi.models.user import DBUser
from fabrikApi.plugins.CIR.assembly.lib import compose_public_cir_profile, get_personal_rating, is_personal_statement
from fabrikApi.plugins.CIR.certificates import CERTIFICATE_ARGUMENTREPERTOIRE, \
    CERTIFICATE_FIRST_PEERREVIEWING, \
    CERTIFICATE_OUTLINING_OWN_SIDE, CERTIFICATE_BOTH_SIDE_ENGAGEMENT, CERTIFICATE_USER_DISCUSSION, \
    NUMBER_OF_INTERACTIONS_BEFORE_ACCOMPLISH_CERTIFICATES
from fabrikApi.util.constants import NOTIFICATION_ACTION_CERTIFICATE_ACCOMPLISHED, \
    NOTIFICATION_ACTION_PEERREVIEW_ASSIGNED
from fabrikApi.util.events import EventAccomplishedCertificate, EventContentCreated, EventContentEdited, \
    EventContentInteract, EventPeerReviewInitialized, \
    EventPeerreviewManagerInitialization, EventPeerreviewResponseSubmitted, \
    EventViewAssemblyUser


@subscriber(EventViewAssemblyUser)
def event_raised_when_showing_assembly_user_public_profile(event):

    # reorder of siblings if this is random-order content.
    assert event.request, "request is missing ---."
    assert event.response, "response object is missing"

    request = event.request
    response = event.response

    if request.assembly.type_ != 'CIR':
        # IGNORE IF ITS NOT A CIR-ASSEMBLY
        return None

    user_id = request.assembly_user.id
    assert user_id

    compose_public_cir_profile(request, user_id, response)


@subscriber(EventPeerreviewManagerInitialization)
def notified_after_treemanager_initialization(event):

    # reorder of siblings if this is random-order content.
    assert event.request, "request is missing ---."
    assert event.response, "response is missing ---."
    assert event.peerreview_manager, "peerreview manager is missing"

    request = event.request
    peerreview_manager = event.peerreview_manager

    # patched required.
    assert request.assembly.patched

    # continue only for delegates
    if not event.request.has_delegate_permission(event.request.assembly.identifier):
        return None

    assembly_progression = get_assembly_progression_of_current_user(request)

    # Assign Open Peerreviews to the user...
    nof_new_peerreviews = peerreview_manager.assign_open_peerreviews(
        assembly_progression=assembly_progression,
        prefer_current_stage=False)

    if nof_new_peerreviews is None:
        return None

    # add notification
    if nof_new_peerreviews > 0:
        notification = initiate_notification(
            request=event.request,
            action=NOTIFICATION_ACTION_PEERREVIEW_ASSIGNED,
            user_id=event.request.local_userid,
            assembly=event.request.assembly,
            # stage=event.stage,
            contenttree_id=peerreview_manager.contenttree.id if peerreview_manager.contenttree.id else None,
            contenttree_identifier="%s" % peerreview_manager.contenttree.id if peerreview_manager.contenttree else None,
            value=nof_new_peerreviews)

        # straight forward: return to user...
        if event.response:
            if not event.response.get('notifications'):
                event.response['notifications'] = {}
            event.response['notifications'][notification.id] = notification


@subscriber(EventAccomplishedCertificate)
def notified_after_certificate_accomplished(event):
    if event.user_id:
        user = event.request.dbsession.query(DBUser).get(event.user_id)
    else:
        user = event.request.current_user
    assert user
    custom_data = user.custom_data

    # abort, when user does not have enabled certificates...
    if not custom_data.get('ENABLE_MEDALS'):
        return None

    # get already accomplished certificates from  DBUSer model (custom_data)
    assert custom_data
    if "CERTIFICATES" not in custom_data:
        custom_data['CERTIFICATES'] = []

    # Certificate already received?
    if event.certificate in custom_data['CERTIFICATES']:
        return None

    # add new certificate

    is_first_certificate = len(custom_data['CERTIFICATES']) == 0

    custom_data['CERTIFICATES'].append(event.certificate)
    custom_data["LAST_UPDATE"] = "%s" % arrow.utcnow()

    # add notification
    notification = initiate_notification(
        request=event.request,
        action=NOTIFICATION_ACTION_CERTIFICATE_ACCOMPLISHED,
        user_id=user.id,
        assembly=event.request.assembly,
        value=event.certificate)

    if is_first_certificate:
        notification.call_action_when_initialized = True

    if event.request.local_userid != user.id:
        return None

    # PROGRESS ONLY IF CERTIFICATE GOES TO CURRENT USER
    # straight forward: return to user...
    assert event.response
    if not event.response.get('notifications'):
        event.response['notifications'] = {}
    event.response['notifications'][notification.id] = notification
    # event.response['certificates'] = custom_data.get('CERTIFICATES', [])


@subscriber(EventContentCreated, EventContentEdited)
def notified_after_a_content_has_been_created_or_modified(event):

    # CERTIFICATE: OUTLINE OWN POSITION
    custom_data = event.request.assembly.custom_data
    statement_contenttree_id = custom_data['STATEMENT_CONTENTTREE_ID']
    if is_personal_statement(event.request, statement_contenttree_id, event.content):
        raiseEvent = EventAccomplishedCertificate(
            request=event.request,
            response=event.response,
            certificate=CERTIFICATE_OUTLINING_OWN_SIDE)
        event.request.registry.notify(raiseEvent)
        return None

    if event.content.parent_id:

        # NOTIFY creator of parent
        parent = event.request.dbsession.query(
            DBContent).get(event.content.parent_id)
        raiseEvent = EventAccomplishedCertificate(
            request=event.request,
            user_id=parent.user_created_id,
            response=event.response,
            certificate=CERTIFICATE_USER_DISCUSSION)
        event.request.registry.notify(raiseEvent)


@subscriber(EventPeerReviewInitialized)
def notified_after_a_peerreview_has_been_initialized(event):

    # CERTIFICATE: MODIFIED NEW ARGUMENT
    # procon_contenttree_id = custom_data['PROS_AND_CONS_CONTENTTREE_ID']
    # if procon_contenttree_id == event.content.evencontenttree_id:
    #     if event.content.type_ in ["PRO", "CONTRA"]:
    raiseEvent = EventAccomplishedCertificate(
        request=event.request,
        response=event.response,
        certificate=CERTIFICATE_ARGUMENTREPERTOIRE)
    event.request.registry.notify(raiseEvent)


@subscriber(EventPeerreviewResponseSubmitted)
def notified_when_peerreview_is_responded(event):
    raiseEvent = EventAccomplishedCertificate(
        request=event.request,
        response=event.response,
        certificate=CERTIFICATE_FIRST_PEERREVIEWING)
    event.request.registry.notify(raiseEvent)


@subscriber(EventContentInteract)
def notified_when_pro_or_contra_content_is_read(event):

    side = None

    # Stop here for guests
    if not event.request.local_userid:
        return None

    # Ignore own content
    if event.request.local_userid == event.content.user_created_id:
        return None

    # Either: position of argument
    if event.content.type_ in ('CONTRA', 'PRO'):
        side = event.content.type_

    # OR INVITED_COMMENTS (Public Statements):  position of autor
    if event.content.type_ == 'INVITED_COMMENT':
        user_id = event.content.user_created_id
        rating = get_personal_rating(event.request, user_id)
        if rating is not None:
            if rating > 50:
                side = 'PRO'
            elif rating < 50:
                side = 'CONTRA'

    # STORE CONTENT ID TO LIST OF INTERACTED CONTENT
    if not side:
        # Ignore
        return None

    # get already accomplished certificates from  DBUSer model (custom_data)
    custom_data = event.request.current_user.custom_data
    assert custom_data
    if "INTERACTIONS" not in custom_data:
        custom_data["INTERACTIONS"] = {'PRO': [], 'CONTRA': []}
    if event.content.id in custom_data["INTERACTIONS"][side]:
        # already in
        return None

    custom_data["INTERACTIONS"][side].append(event.content.id)
    custom_data["LAST_UPDATE"] = "%s" % arrow.utcnow()

    # if len(custom_data["INTERACTIONS"][side]) >= NUMBER_OF_INTERACTIONS_BEFORE_ACCOMPLISH_CERTIFICATES:
    #     if side == "PRO":
    #         certificate = CERTIFICATE_PRO_SIDE_ENGAGEMENT
    #     if side == "CONTRA":
    #         certificate = CERTIFICATE_CONTRA_SIDE_ENGAGEMENT

    if len(custom_data["INTERACTIONS"]["PRO"]) >= NUMBER_OF_INTERACTIONS_BEFORE_ACCOMPLISH_CERTIFICATES \
            and len(custom_data["INTERACTIONS"]["CONTRA"]) >= NUMBER_OF_INTERACTIONS_BEFORE_ACCOMPLISH_CERTIFICATES:

        raiseEvent = EventAccomplishedCertificate(
            request=event.request,
            response=event.response,
            certificate=CERTIFICATE_BOTH_SIDE_ENGAGEMENT)
        event.request.registry.notify(raiseEvent)
