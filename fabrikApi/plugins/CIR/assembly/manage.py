
import arrow
from sqlalchemy import text
from fabrikApi.models.contenttree.contenttree import DBContentTree
from fabrikApi.models.user import DBUser
from fabrikApi.util.contenttree_manager import ContentTreeManager
from fabrikApi.views.lib.factories import AssemblyFactory
from cornice.service import Service

manage_voices_service = Service(
    cors_origins=('*',),
    name='manage_voices',
    path='/assembly/{assembly_identifier}/manage_voices',
    traverse='/{assembly_identifier}',
    description='Show voices to manage',
    factory=AssemblyFactory)


@manage_voices_service.post(permission='manage')
def manage_voices(request):
    """
    Show unsigned Content
    """

    users = request.dbsession.query(DBUser).all()
    users_by_id = {}
    for user in users:
        users_by_id[user.id] = user

    andfilter = ' 1=1 '
    f = None
    # TODO: validate jsonschema to  protect for sql injection also for moderators...
    if request.json_body and request.json_body['filter']:
        f = f"%{request.json_body['filter'].lower()}%"
        andfilter = " (LOWER(title) LIKE :andfilter OR LOWER(text) LIKE :andfilter OR id LIKE :andfilter) "
    if request.json_body and request.json_body.get('filterToggle'):
        filterToggle = request.json_body['filterToggle']
        if filterToggle == 'statement':
            andfilter = f"(({andfilter}) AND LOWER(text) NOT LIKE '')"
        if filterToggle == 'toreview':
            andfilter = f"(({andfilter}) AND reviewed=0)"

    statement = text(f"""
        SELECT content.id, content.contenttree_id, content.title, SIGNED.*, content.user_created_id
        FROM
            content
        INNER JOIN
                (SELECT  subtree_content_id, count(*) as content_N, sum(reviewed=0) as unreviewed, max(date_created) as last_created, max(date_modified) as last_modified FROM content  WHERE deleted <> 1 AND disabled <> 1 AND subtree_content_id IS NOT NULL AND {andfilter} GROUP BY subtree_content_id) SIGNED
        ON content.id = SIGNED.subtree_content_id
        WHERE deleted <> 1 AND disabled <> 1 AND contenttree_id IN (SELECT id FROM contenttree WHERE deleted <> 1 AND disabled <> 1 AND  assembly_id = (SELECT id FROM assembly WHERE identifier = :identifier));
        """)
    if f:
        torun = statement.bindparams(
            andfilter=f,
            identifier=request.assembly.identifier)
    else:
        torun = statement.bindparams(
            identifier=request.assembly.identifier)
    results = request.dbsession.execute(torun)

    # request.dbsession.execute
    data = [dict(row) for row in results.fetchall()]

    for row in data:
        rounded = round(row['unreviewed'], 0) if row['unreviewed'] else None
        row['unreviewed'] = int(f'{rounded}') if row['unreviewed'] else None
        row['last_created'] = arrow.get(row['last_created'])
        row['last_modified'] = arrow.get(row['last_modified'])
        row['user'] = users_by_id[row['user_created_id']]

    return {
        "voice": data
    }


manage_arguments_service = Service(
    cors_origins=('*',),
    name='manage_arguments',
    path='/assembly/{assembly_identifier}/manage_arguments',
    traverse='/{assembly_identifier}',
    description='Show arguments to manage',
    factory=AssemblyFactory)


@manage_arguments_service.post(permission='manage')
def manage_arguments(request):
    """
    Show unsigned Content!
    """

    # filter
    commentfilter = None
    if request.json_body and request.json_body['filter']:
        commentfilter = request.json_body['filter'].lower()

    users = request.dbsession.query(DBUser).all()
    users_by_id = {}
    for user in users:
        users_by_id[user.id] = user

    # Load ContentTree Content
    contenttree_id = request.assembly.custom_data.get(
        'PROS_AND_CONS_CONTENTTREE_ID')
    assert contenttree_id
    contenttree = request.dbsession.query(DBContentTree).get(contenttree_id)
    treema = ContentTreeManager(
        request, contenttree=contenttree, subtree_content_id=None)
    contenttree = treema.load_content()

    argbykey = {}
    for content_id in contenttree.get("entries").keys():
        contenttuple = contenttree.get("entries")[content_id]
        content = contenttuple.get('content')

        if content.deleted or content.disabled:
            continue

        if commentfilter and\
                f'{content.id}'.find(commentfilter) == -1\
                and content.title.lower().find(commentfilter) == -1\
                and content.text.lower().find(commentfilter) == -1:
            continue

        rootid = contenttuple.get('path')[0]
        if rootid not in argbykey:
            root_el = contenttree.get("entries")[rootid]
            root_content = root_el.get("content")
            argbykey[rootid] = {
                'unreviewed': 0,
                "content_N": 0,
                "id": rootid,
                "peerreview_decision": "Acc" if root_content.completed_peerreview_for_insert else ("Rej" if root_content.rejected_peerreview_for_insert else ""),
                "rating_avg":  root_content.agg_rating_avg,
                "rating_count": root_content.agg_rating_count,
                "title": root_content.title,
                "text": root_content.text,
                "type": root_content.type_,
                "user": users_by_id[root_content.user_created_id],
                "contenttree_id": contenttree_id,
                "last_created": arrow.get(root_content.date_created),
                "date_created": arrow.get(root_content.date_created),
                "last_modified": arrow.get(root_content.date_modified)
            }
        if content.reviewed == 0:
            argbykey[rootid]['unreviewed'] += 1
        argbykey[rootid]['content_N'] += 1
        if content.date_created > argbykey[rootid]['last_created']:
            argbykey[rootid]['last_created'] = arrow.get(content.date_created)
        if content.date_modified > argbykey[rootid]['last_modified']:
            argbykey[rootid]['last_modified'] = arrow.get(
                content.date_modified)

    return {
        "arguments": list(argbykey.values())
    }
