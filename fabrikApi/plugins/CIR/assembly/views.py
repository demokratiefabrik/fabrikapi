
# SERVICES
import math

import arrow
from fabrikApi.models.log import DBLog
from fabrikApi.plugins.CIR.assembly.cirplot import get_random_data, get_user_data, xmlPostModifications
from fabrikApi.plugins.CIR.etc.plots.polarbee import Compass
from fabrikApi.views.lib.factories import AssemblyFactoryPublic
from cornice.service import Service


cirplot = Service(
    cors_origins=('*',),
    name='cirplot',
    # renderer='string',
    # content/{content_id: \d+}
    path='/assembly/{assembly_identifier}/cirplot',
    # content_type='image/svg+xml',
    traverse='/{assembly_identifier}',
    description='Show Assembly Plot.',
    factory=AssemblyFactoryPublic)


@cirplot.get(permission='public')
def cir(request):
    """
    Return current cirplot
    """

    # get position of own entry
    # test
    currentUserID = request.local_userid
    # currentUserID = 1672

    # a0 = time.perf_counter()

    config = {
        "markerSizeFactor": float(request.GET.get('marker-size-factor', 1)),
        "markerColorMap": request.GET.get('marker-color-map', 'winter'),
        "xAxisLabels":  [
            (math.radians(0), 'Kontra', 'end'),
            (math.radians(90), 'Unentschieden', 'center'),
            (math.radians(180), 'Pro', 'start')
        ]
    }

    distribution = request.GET.get('distribution', 'uniform')

    assert request.assembly
    topic_content_id = request.assembly.custom_data.get(
        'TOPIC_CONTENT_ID', None)
    assert topic_content_id

    if distribution.startswith('default'):
        totalUserCount, users = get_user_data(request, currentUserID)
    else:
        totalUserCount, users = get_random_data(request)

    userpositions = list(map(lambda x: x['pos'],  users))

    # a = time.perf_counter()
    # print('Timer.data.init', )
    # print(round(a-a0, 2))

    compass = Compass(data=userpositions, config=config)

    # b = time.perf_counter()
    # print('Timer.positioning', )
    # print(round(b-a, 2))

    xml = compass.plotAsXml()

    # c = time.perf_counter()
    # print('Timer.plot', )
    # print(round(c-b, 2))

    # selectedNodeIds = [currentUserID]  # this is the current user...
    output = xmlPostModifications(xml, users, currentUserID)

    # d = time.perf_counter()
    # print('Timer.xml', )
    # print(round(d-c, 2))

    # log entry
    elog = DBLog(action='ANALYTICS')
    elog.value = "PAGE_LOAD"
    elog.date_created = arrow.utcnow()
    request.dbsession.add(elog)

    return {
        "svg": output.decode("utf-8"),
        "totalUserCount": totalUserCount,
        # "plottedUser": None,
        "plottedUserCount": len(users)
    }
