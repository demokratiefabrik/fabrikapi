"""
Polar/Tacho/Swarmplot projection.
"""

from io import BytesIO
import math
# import time
import matplotlib.textpath as textpath
import matplotlib.pyplot as plt
import numpy as np

plt.rcParams['svg.fonttype'] = 'none'  # use font installed on client


def balancedArange(start, stop, step):
    """ replication of np.arange """
    number = math.floor((stop-start)/(step))
    evenly_spaced = [start + i * step for i in range(number+1)]
    return (evenly_spaced)


class Compass:
    config = None
    ceilingy = None
    outerY = None
    freeSlotsByY = None
    reversedFreeSlotsByY = None
    dotsByY = None
    dots = None
    rowsByY = None
    fig = None  # matplotlib figure
    helperFirstRowSlotAngle = None

    # TODO: is ceilingy not same as outerY

    def __init__(self, data=None, config={}):

        self.dots = []
        self.rowsByY = {}
        self.freeSlotsByY = {}  # free slots...
        self.reversedFreeSlotsByY = {}  # free slots...in reverse order for faster lookup
        self.dotsByY = {}  # free slots...
        self.ceilingy = 0
        self.config = {
            "markerSizeFactor": 1,  # factor to scale the markers (default 1)
            # factor to scale the markers (default 1)
            "markerColorMap": 'winter',
            # lowest y-value shown (center of the circle remains empty)
            "innerY": 2,
            "xAxisLabels": [],  # e.g. [(radians(90), 'Middle')]
            "xAxisLabelsStyle": {"size": 30},  # as px (i assume)
            "chartAngle": 180,  # angle of the polar chart (180 = half-circle)
            "forceJustification": False,  # force left/rigth justifications of dots on each y-line
            "xAxisMax": 100,  # scale from 0 to 100
            "yAxisOuterPadding": 1,  # append empty space at the end of the y-axis
            "minOuterY": 20,  # polar plots are plotted with atleast ylim(,20)
            # the chart is zoomed in (because only the on half of the polar plot is used)
            "zoomFactor": 3.6/10,
            # "maxOuterY" : 100 # NOT YET IMPLEMENTED
        }

        # update runtime config
        self.config.update(config)

        # assert configuration
        assert self.config['innerY'] > 0
        assert self.config['minOuterY'] >= 0
        assert self.config['chartAngle'] > 0
        assert self.config['forceJustification'] in [True, False]

        self.outerY = self.config['minOuterY']

        # generate blank slotes for baseline
        self.generateBlankSlotRow(self.config['innerY'])

        # Add Data
        self.addValues(data)

    def generateBlankSlotRow(self, y):
        """generate empty slots for new y row"""

        if y > self.outerY:
            # maximal y value reached...
            return None

        if y <= self.ceilingy:
            # slots for this row are already defined...
            return None

        assert y > self.ceilingy, (y, self.ceilingy)
        assert y >= self.config['innerY']

        row = CompassRow(y, self)
        self.rowsByY[y] = row
        dotList = row.getDotList()
        self.freeSlotsByY[y] = dotList
        self.reversedFreeSlotsByY[y] = dotList[::-1]
        self.dotsByY[y] = []
        self.ceilingy = y

        if y == self.config['innerY']:
            self.helperFirstRowSlotAngle = row.slotAngleWidth

    def getAngleByX(self, x):
        return 1/self.config['xAxisMax']*x * self.config['chartAngle']

    def addValues(self, values):
        """ assign a data observation to a slot in the dotplot"""

        for value in values:
            assert round(value) in range(0, 101), value
            angle = self.getAngleByX(value)
            self.assignValueToAFreeSlot(angle, value)

    def assignValueToAFreeSlot(self, angle, value, skipBelowY=0):
        """ Assign value to a free dot-slot at a near position. """

        # It is not always needed to start at first y-row. skipBelowY
        startY = max(skipBelowY, self.config['innerY'])
        previous = None

        # a list of second choice candidates. Neighbors without an exact match!!
        secondChoiceSlots = []
        reverseWalk = angle > (self.config['chartAngle']/2)

        # FIRST CHOICES: Perfect Position Matchs
        for y in range(startY, self.ceilingy+1):
            if previous:
                secondChoiceSlots.append(previous)
                previous = None

            if reverseWalk:
                freeRowSlots = self.reversedFreeSlotsByY[y]
            else:
                freeRowSlots = self.freeSlotsByY[y]
            for freeslot in freeRowSlots:
                if reverseWalk:
                    if not freeslot.fits_lower_limit(angle):
                        previous = freeslot
                        continue
                    if not freeslot.fits_upper_limit(angle):
                        secondChoiceSlots.append(freeslot)
                        break
                else:
                    if not freeslot.fits_upper_limit(angle):
                        previous = freeslot
                        continue
                    if not freeslot.fits_lower_limit(angle):
                        secondChoiceSlots.append(freeslot)
                        break
                self.registerSlot(freeslot, value)
                return True

        # SECOND CHOICES: Imperfect Position Matchs:
        # i.e. behind first row dot, with perfect position match
        if len(secondChoiceSlots) > 0:

            # preference of close as possible matches...
            secondChoiceSlots.sort(key=lambda dot: abs(angle - dot.angle))

            for nearDot in secondChoiceSlots:

                # necessary condition: must be behind first row dot, with perfect match
                if not (nearDot.angle-self.helperFirstRowSlotAngle < angle <= nearDot.angle+self.helperFirstRowSlotAngle):
                    continue

                # must be based on another already selected dot on y-1 (respectively or must be the baseline)
                # if nearDot.check_secondary_choice_candidate(angle):
                if nearDot.y == self.config['innerY'] or any(baseDot.overlapping(nearDot.angle) for baseDot in self.dotsByY[nearDot.y-1]):
                    # print("DEBUG: secondary choice for angle %s on l2 %s on pos%s" %
                    #       (angle, nearDot.y, nearDot.angle))
                    self.registerSlot(nearDot, value, exactPlacement=False)
                    return True

        # NO MATCH: Extend the plot range
        # print("DEBUG: teriary choice: extend plot")
        self.outerY += 1
        # if (self.outerY >= 10):
        #     print(self.outerY)

        self.generateBlankSlotRow(self.outerY)
        if reverseWalk:
            newSlots = self.reversedFreeSlotsByY[self.outerY]
            newDot = next(
                obj for obj in newSlots if obj.fits_lower_limit(angle))
            assert newDot
            assert newDot.fits_upper_limit(angle)
        else:
            newSlots = self.freeSlotsByY[self.outerY]
            newDot = next(
                obj for obj in newSlots if obj.fits_upper_limit(angle))
            assert newDot
            assert newDot.fits_lower_limit(angle)

        # Take first choice slot from new row.
        self.registerSlot(newDot, value, exactPlacement=False)
        return True
        #  return self.assignValueToAFreeSlot(angle, value, skipBelowY=self.outerY)

    def registerSlot(self, slot, value, exactPlacement=True):
        slot.value = value
        slot.exactPlacement = exactPlacement
        slot.index = len(self.dots)
        self.dots.append(slot)
        self.dotsByY[slot.y].append(slot)
        self.freeSlotsByY[slot.y].remove(slot)
        self.reversedFreeSlotsByY[slot.y].remove(slot)

        # create slots for row above..
        self.generateBlankSlotRow(slot.y + 1)

    def plot(self):

        self.fig = plt.figure(frameon=False, dpi=400, figsize=(10, 10))

        # POLAR CHART CANVAS
        ax = self.fig.add_subplot(projection='polar', aspect=1)
        ax.set_xlim(0, math.radians(self.config['chartAngle']))
        ax.set_ylim(self.config['innerY']-0.5, self.outerY +
                    0.5+self.config['yAxisOuterPadding'])
        ax.set_rorigin(-self.config['innerY'])
        ax.set_axisbelow(True)  # put grid behind dots
        plt.xticks([], [])
        ax.axes.yaxis.set_ticklabels([])
        # ax.axes.yaxis.set_ticks([10,11,12],['a','b','c'])

        # ADAPT FIGURE SIZE (zoom in to remove margins...)
        ext = self.fig.gca().get_window_extent()
        self.fig.set_figheight(ext.height/self.fig.dpi)
        self.fig.set_figwidth(ext.width/self.fig.dpi*2)
        zoomUnits = {
            "top": 1+self.config['zoomFactor'],
            "bottom": - self.config['zoomFactor'],
            "left": -self.config['zoomFactor'],
            "right": 1+self.config['zoomFactor']
        }
        self.fig.subplots_adjust(**zoomUnits)

        # Draw Canvas before convert any pixel to font size etc...
        self.fig.canvas.draw()

        # x-Axis LABELS
        for pos, label, align in self.config['xAxisLabels']:
            self._matplotlib_curvedText(
                ax, radianX=pos, y=self.outerY+1+self.config['yAxisOuterPadding'], text=label, align=align, style=self.config['xAxisLabelsStyle'])

        # MARKERS

        # can we do it more efficient?
        # [[math.radians(dot.angle)], [math.radians(dot.angle)] for dot in self.dots]
        # x = [math.radians(dot.angle) for dot in self.dots]
        # c = [dot.value for dot in self.dots]
        # y = [dot.y for dot in self.dots]

        dotParams = {"x": [], "y": [], "c": []}
        for dot in self.dots:
            dotParams["x"].append(math.radians(dot.angle))
            dotParams["c"].append(dot.value)
            dotParams["y"].append(dot.y)

        points = self._matplotlibGetYUnitInPoints(
            ax)*self.config['markerSizeFactor']

        ax.scatter(
            gid='scatgrid', s=points**2,
            alpha=0.75,
            cmap=self.config['markerColorMap'], **dotParams)
        #    cmap=self.config['markerColorMap'], x=x, y=y, c=c)

    def plotAsXml(self):

        # c0 = time.perf_counter()

        self.plot()

        # c1 = time.perf_counter()
        # print('Timer.plot.plot', )
        # print(round(c1-c0, 2))

        f = BytesIO()
        self.fig.savefig(f, format='svg', dpi=self.fig.dpi,  transparent=True)
        xml = f.getvalue()
        f.truncate(0)  # empty stream again

        # c2 = time.perf_counter()
        # print('Timer.plot.file', )
        # print(round(c2-c1, 2))

        return xml

    def _matplotlibGetYUnitInPixel(self, ax):
        start = ax.viewLim.min[1]
        posMin = ax.transData.transform((math.pi/2, start))
        posMax = ax.transData.transform((math.pi/2, start+1))
        a = posMax[0] - posMin[0]
        b = posMax[1] - posMin[1]
        return math.sqrt(a**2+b**2)

    def _matplotlibGetYUnitInPoints(self, ax):
        return self._matplotlibGetYUnitInPixel(ax) * (72./ax.figure.dpi)

    def _matplotlibGetPointsInPixel(self, size):
        return size/(72./self.fig.dpi)

    def _matplotlib_get_window_extent(self):
        return self.fig.get_window_extent().width, \
            self.fig.get_window_extent().height

    def _matplotlib_get_polar_chart_position(self):
        fh, fw = self._matplotlib_get_window_extent()
        leftBottom = self.fig.gca().transData.transform(
            (math.pi, self.outerY+0.5+self.config['yAxisOuterPadding']))
        centerTop = self.fig.gca().transData.transform(
            (math.pi/2, self.outerY+0.5+self.config['yAxisOuterPadding']))
        x = centerTop[0]
        r = centerTop[0]-leftBottom[0]
        y = fh/2-leftBottom[1]
        return x, y, r

    def _matplotlib_curvedText(self, ax, radianX, y, text, align, style={}):

        transformation_rate = ax.figure.dpi/72

        curvedSize = style.get('size') if style.get('size') else 40
        curvedStyle = {"rotation_mode": 'anchor', "transform_rotates_text": True,
                       "ha": 'left', "rotation": 180, "size": curvedSize}
        curvedStyle.update(style)

        # identify pixel  per degree at this y-position
        pixelpos0 = ax.transData.transform((math.radians(0), y))
        pixelpos1 = ax.transData.transform((math.radians(0.001), y))
        a = pixelpos0[0] - pixelpos1[0]
        b = pixelpos0[1] - pixelpos1[1]
        pixelPerDegree = math.sqrt(a**2+b**2) * 1000
        degreePerPixel = 1/pixelPerDegree

        # Positioning chars and take char widths
        # set cursor to the beginning of the word (half-word distance from x)
        letterSpace = 7
        chars = []
        wordWidth = 0
        wordSpace = 0
        for char in text:
            c = {'text': char}
            tmpPath = textpath.TextPath(
                (0, 0), char, size=curvedStyle['size'])
            c['width'] = tmpPath.get_extents().width * transformation_rate
            chars.append(c)
            wordWidth += c['width']

            # add extra space for i and 1
            c['space'] = letterSpace + letterSpace * (char == 'i')
            wordSpace += c['space']

        # remove word-trailing space again:
        wordSpace -= c['space']
        # sum up letter spaces
        wordWidth += wordSpace

        # calculate exact position
        if align == 'end':
            alignGap = math.radians(wordWidth*degreePerPixel)
        elif align == 'start':
            alignGap = 0
        else:
            alignGap = math.radians(wordWidth/2*degreePerPixel)
        cursorPosition = radianX + alignGap
        for c in chars:
            ax.text(cursorPosition, y, c['text'], **curvedStyle)
            cursorPosition -= math.radians((c['width'] +
                                           c['space'])*degreePerPixel)


class CompassDot:
    value = None  # value on 1:100 scale
    x = None  # position in on x-axis (can differ from value...)
    y = None  # row number
    angle = None   # angle (position) number (on X-axis)
    # how wide is the angle used for this dot. (depending on y position)
    angleWidth = None
    isUpperBoundary = False
    isLowerBoundary = False
    index = None
    compass = None

    def __init__(self, value, x, y, angle, angleWidth, compass):
        self.value = value
        self.x = x
        self.y = y
        self.angle = angle
        self.angleWidth = angleWidth
        self.compass = compass

    def overlapping(self, angle):
        """ does current slot overlapps with given angle...
        # TODO: if tara is true: also check if angle overlapps with gap besides dot.
        """
        # TODO <=
        # corrected also for row intent at boundary dots (with no direct overlapp)
        # TODO correct also for row intent at boundary dots (with no direct overlapp)
        lowerlimit = self.fits_lower_limit(angle)
        upperlimit = self.fits_upper_limit(angle)
        return (lowerlimit and upperlimit)

    def fits_lower_limit(self, angle):
        """ does current slot overlapps with given angle...
        # TODO: if tara is true: also check if angle overlapps with gap besides dot.
        """
        lowerlimit = -1000 if self.isLowerBoundary else self.angle-self.angleWidth/2
        return angle > lowerlimit

    def fits_upper_limit(self, angle):
        """ does current slot overlapps with given angle...
        # TODO: if tara is true: also check if angle overlapps with gap besides dot.
        """
        upperlimit = 1000 if self.isUpperBoundary else self.angle+self.angleWidth/2
        return angle <= upperlimit
        # or self.isUpperBoundary

    # def overlappingWithTolerance(self, angle):
    #     """ does current slot overlapps with toleranceAngle...
    #     toleranceAngle: angle of dots at the innerY radius """
    #     # self.config['toleranceAngle']
    #     firstRow = self.compass.rowsByY[self.compass.config.get('innerY')]
    #     toleranceAngle = firstRow.slotAngleWidth
    #     return (angle > self.angle-toleranceAngle and angle <= self.angle+toleranceAngle)

    # def check_secondary_choice_candidate(self, angle):
    #     if not self.overlappingWithTolerance(angle):
    #         return False

    #     return (
    #         self.y == self.compass.config['innerY'] or any(baseDot.overlapping(self.angle) for baseDot in self.compass.dotsByY[self.y-1]))


class CompassRow:
    """ Where should the dots be places on each row? """

    y = None  # value on 1:100 scale

    innerMargin = None   # angle (position) number (on X-axis)
    angleWidth = None
    chartAngle = None
    dotList = None
    compass = None

    # calculated
    nofRowSlots = None
    slotAngleWidth = None  # angleWidth of the dots on this row
    gapAngleWidth = None  # angleWidth of the gap between the dots

    def __init__(self, y, compass):
        self.compass = compass
        self.y = y
        self.slotNumbers()

    def slotNumbers(self):
        """ how many dots can be aligned on this y (radius)"""
        yhalf = self.y + (self.compass.config.get('innerY')
                          )  # add space in the inner circle
        rowSlotsUnrounded = yhalf*2 * math.pi / 2
        self.nofRowSlots = math.floor(rowSlotsUnrounded)
        gapSum = rowSlotsUnrounded - self.nofRowSlots

        # What is the angle spread for a dot (depending on y). (how big is the piece of cake)

        # calculate gap and slot angles
        if not self.compass.config.get('forceJustification'):
            # dont leave space between the dots
            self.slotAngleWidth = self.compass.config.get(
                'chartAngle') / self.nofRowSlots
            self.gapAngleWidth = 0

        if self.compass.config.get('forceJustification'):
            # leave space between the dots
            nofRowGaps = self.nofRowSlots - 1
            slotGap = gapSum / self.nofRowSlots
            # calculated by: gapAngleWidth = slotAngleWidth * slotGap
            self.slotAngleWidth = self.compass.config.get(
                'chartAngle') / (self.nofRowSlots + nofRowGaps*slotGap)
            self.gapAngleWidth = self.slotAngleWidth*slotGap

        # Check: gapAngleWidth * nofRowGaps + slotAngleWidth * nofRowSlots

    def getXByAngle(self, angle):
        return 1/180*angle * self.compass.config.get('xAxisMax')

    def getDotList(self):
        self.dotList = []

        if self.compass.config['forceJustification']:
            start = self.slotAngleWidth/2

        if not self.compass.config['forceJustification']:
            # Center dot-row on the x-axis
            maxAlignGap = ((self.compass.config.get('chartAngle')) -
                           (self.slotAngleWidth*(self.nofRowSlots)))
            # maxAlignGap*np.random.random()
            alignGap = maxAlignGap * (self.y % 2)
            start = self.slotAngleWidth/2 + alignGap

        slotList = list(
            np.arange(
                start=start,
                stop=self.compass.config['chartAngle'],
                step=self.slotAngleWidth+self.gapAngleWidth))[:self.nofRowSlots]
        # slotList = list(
        #     balancedArange(
        #         start=start,
        #         stop=self.compass.config.get('chartAngle'),
        #         step=self.slotAngleWidth+self.gapAngleWidth))[:self.nofRowSlots]
        dotList = list(map(lambda angle: CompassDot(
            value=None,
            x=self.getXByAngle(angle),
            y=self.y,
            angle=angle,
            angleWidth=self.slotAngleWidth,
            compass=self.compass), slotList))
        # dotList = list(map(lambda angle: angle, slotAngleWidth), slotList))
        # self.getXByAngle(slotList[1]) - self.getXByAngle(slotList[0])
        dotList[0].isLowerBoundary = True
        dotList[-1].isUpperBoundary = True
        return dotList
