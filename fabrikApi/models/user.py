# -*- coding: utf-8 -*-
# flake8: noqa

"""
Register all users. (saves all data that has been transferred from oAUth servers)
Like username, email etc..

"""

from fabrikApi.util.authorization import getDefaultObjectACLs
from fabrikApi.util.lib import convert_language_to_ISO639_1, get_default_language_code, number_of_days_passed
import logging

from sqlalchemy import Column, Integer, Unicode, or_, Index, Float, JSON
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy_utils import ArrowType

from fabrikApi.models.lib.meta import Base
from fabrikApi.models.lib.mixins import BaseDefaultObject
from pyramid.decorator import reify

logger = logging.getLogger(__name__)

__all__ = ['DBUser']

NO_EMAIL_NOTIFICATION = 0
RARE_EMAIL_NOTIFICATION = 1
SOME_EMAIL_NOTIFICATION = 2
FULL_EMAIL_NOTIFICATION = 3


class DBUser(BaseDefaultObject, Base):

    """
    STORES ALL PARTICIPANTS; ADMINISTRATORS AND MODERATORS
    """
    __roles__ = None
    __name__ = None

    # Table Definition
    __tablename__ = "user"
    __table_args__ = (
        # Ensure unique progression for each user:
        Index("uq_user_oauth2userid_oauth2provider", "oauth2_user_id", "oauth2_provider",
              unique=True),
    )

    # primaries & uniques
    id = Column(Integer, primary_key=True)
    oauth2_provider = Column(Unicode(150), nullable=False)
    oauth2_user_id = Column(Integer, nullable=False)
    username = Column(Unicode(100), default=None)  # username
    custom_data = Column(MutableDict.as_mutable(JSON), nullable=True)

    # to show application tutorial at the firsttime
    # platform_introduction_passed = Column(Boolean, default=False)
    # artificial_moderation_enabled = Column(Boolean, default=True)
    notification_email_frequency = Column(
        Integer, default=SOME_EMAIL_NOTIFICATION)

    # STATS
    date_last_interaction = Column(ArrowType)  # assembly
    content_rating_count = Column(Integer, default=0)
    content_salience_count = Column(Integer, default=0)
    content_certainity_count = Column(Integer, default=0)
    content_created_count = Column(Integer, default=0)

    # more stats values (either calculated immediatelly, or by async job)
    agg_response_progression_count = Column(Integer)
    agg_response_salience_avg = Column(Float)
    agg_response_salience_count = Column(Integer)
    agg_response_rating_avg = Column(Float)
    agg_response_rating_count = Column(Integer)
    agg_assembly_number_of_day_sessions = Column(Integer)

    def __init__(self, oauth2_provider, oauth2_user_id, username=None, custom_data={}):
        assert oauth2_user_id, "invalid oauth user_id"
        assert oauth2_provider, "invalid oauth2 provider"
        self.oauth2_user_id = oauth2_user_id
        self.oauth2_provider = oauth2_provider
        self.username = username
        self.custom_data = custom_data

    def __str__(self):
        return "User: %s [%s/%s (username: %s)]" % (self.id, self.oauth2_provider, self.oauth2_user_id, self.username)

    def __json__(self, request):

        default_lang = get_default_language_code(request)
        user_lang = convert_language_to_ISO639_1(
            self.custom_data.get('LANG', default_lang))

        response = self.get_response_json(request)

        # TODO: do not transmit certificates if certificates are not enabled for current user...
        response.update({
            'U': self.username,
            'FN': self.custom_data.get('FULLNAME'),
            'CO': self.custom_data.get('COLOR'),
            'LANG': user_lang,
            'ROLE': self.custom_data.get('ROLE'),
            'VAR1': self.custom_data.get('CANTON'),
            'VAR2': self.custom_data.get('ALTITUDE'),
            'VAR3': self.custom_data.get('LENGTH')
        })

        # DELETED: only admins see deleted stages
        if self.deleted:
            assert request.has_administrate_permission, "no administratte permission"

        if self.id == request.local_userid:
            response['CERTIFICATES']: self.custom_data.get('CERTIFICATES', [])

        if request.has_administrate_permission:
            response['CERTIFICATES']: self.custom_data.get('CERTIFICATES', [])
            response["deleted"] = self.deleted
            response["disabled"] = self.disabled

        return (response)

    """ Custom authorization ACLS for contenttree objects..."""
    @reify
    def __acl__(self):
        """ IMPORTANT! use "Allow" only on root object ALSs. (NEVEVER EVERY here, or any other child.)
        # The question to ask: Is there any object-related reason, why the actual request should be denied!
        # By default: this is only the case for Disabled / Deleted / inactive objects
        """

        # acl = self.assembly.__acl__()
        acl = []

        # assembly = find_root(self)
        acl.extend(getDefaultObjectACLs(self))

        # TODO: "append" permission?
        # - rigth to append anywhere in the tree?
        # - right to append on the root level?

        return (acl)

    def setup_lineage(self, request, assembly_context=False):
        if self.__roles__ is not None:
            return None

        if assembly_context:
            # this request is taken in context of a specific assembly.

            self.__parent__ = request.assembly
            self.__local_userid__ = request.local_userid
            self.__assembly_identifier__ = request.assembly.identifier
            self.__parent__.setup_lineage(request)
            self.__roles__ = request.get_auth_roles(self)

        else:

            # leave empty, doc said. but why? (lineage root object),
            self.__name__ = ''
            self.__parent__ = None
            self.__local_userid__ = request.local_userid

        self.__roles__ = request.get_auth_roles(self)

    @property
    def statistics(self):

        # days since last interactivity
        days_since_last_interactivity = 0
        # last_session = self.date_last_interaction
        if self.date_last_interaction:
            days_since_last_interactivity = number_of_days_passed(
                self.date_last_interaction)

        return ({
            'DAYS': self.agg_assembly_number_of_day_sessions,
            'DLI': days_since_last_interactivity,
            'CRC': self.content_rating_and_salience_count,
            'CCC': self.content_created_count,
            'RSC': self.agg_response_salience_count,
            'RSA': self.agg_response_salience_avg
        })

    @property
    def content_rating_and_salience_count(self):
        return int(self.content_rating_count) + int(self.content_salience_count)
